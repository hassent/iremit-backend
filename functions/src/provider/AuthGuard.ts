import {NextFunction, Request, Response} from "express";
import * as admin from 'firebase-admin';

export class AuthGuard {
    static authenticate(req: Request, res: Response, next: NextFunction) {
        console.log(req.query);
        if (req.query.admin) {
            next();
            return;
        }
        console.log("Incoming request: ", req.url, req.method);
        let idToken: string;
        try {
            idToken = req.get('Authorization').split('Bearer ')[1];
        } catch (error) {
            res.status(401).send("Unauthorized");
            return;
        }

        admin.auth().verifyIdToken(idToken)
            .then(decodedToken => {
                console.log(decodedToken);
                const uid = decodedToken.uid;
                console.log(`Authorized with uid ${uid}`);
                if (AuthGuard.authorize(req, uid)) {
                    next();
                } else {
                    res.status(403).send("Unauthorized");
                }

            })
            .catch(err => {
                console.log(err.message);
                res.status(401).send("Unauthorized");
            })

    }

    static authorize(req: Request, uid: string): Boolean {
        if (req.url.indexOf("/users") !== -1) { //currently only user related resources
            switch (req.method) {
                case "POST":
                    return true;
                default:
                    return req.url.indexOf(`/${uid}`) !== -1;
            }
        } else {
            return true
        }
        ;

    }
}