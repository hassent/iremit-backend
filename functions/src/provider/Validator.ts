import {Errors} from "typescript-rest";

type IBuilder<T> = {
        [k in keyof T]: (arg: T[k], strict?: boolean) => IBuilder<T>
    }
    & {
    build(): T
};

export function Builder<T>(): IBuilder<T> {
    const built: any = {};

    const builder = new Proxy(
        {},
        {
            get(target, prop, receiver) {
                if ('build' === prop) {
                    return () => built;
                }
                return (x: any, strict: boolean = true): any => {
                    if (x === undefined){
                        if(strict === true){
                            throw new Errors.BadRequestError(`${prop} is missing in request`);
                        }
                    }else{
                        built[prop] = x;
                    }
                    return builder;
                };
            }
        }
    );

    return builder as any;
}
