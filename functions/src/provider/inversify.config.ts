import "reflect-metadata";
import { Container } from 'inversify';
import TYPES from './Types';

import { UserRepository, UserRepositoryImpl } from "../repository/UserRepository";
import { RemittanceRepository, RemittanceRepositoryImpl } from "../repository/RemittanceRepository";
import { CustomerRepository, CustomerRepositoryImpl } from "../repository/CustomerRepository";
import { DefaultRepository, DefaultRepositoryImpl } from "../repository/defaultRepository";
import { EventRepository, EventRepositoryImpl } from "../repository/EventRepository";
import { RoleRepository, RoleRepositoryImpl } from "../repository/RoleRepository";
import { DepositRepository, DepositRepositoryImpl } from "../repository/DepositRepository";
import { SanctionRepository, SanctionRepositoryImpl } from "../repository/SanctionRepository";

import { UserService, UserServiceImpl } from '../service/UserService';
import { RemittanceService, RemittanceServiceImpl } from '../service/RemittanceService';
import { CustomerService, CustomerServiceImpl } from '../service/CustomerService';
import { DefaultService, DefaultServiceImpl } from '../service/defaultService';
import { EventService, EventServiceImpl } from '../service/EventService';
import { DepositService, DepositServiceImpl } from '../service/DepositService';
import { RoleService, RoleServiceImpl } from '../service/RoleService';
import { SanctionService, SanctionServiceImpl } from '../service/SanctionService';

import { RegistrableController } from '../api/RegisterableController';
import { UserController } from '../api/v1/controller/UserController';
import { RemittanceController } from '../api/v1/controller/RemittanceController';
import { EventController } from '../api/v1/controller/EventController';
import { RoleController } from '../api/v1/controller/RoleController';
import { CustomerController } from '../api/v1/controller/CustomerController';
import { DepositController } from '../api/v1/controller/DepositController';
import { SanctionController } from '../api/v1/controller/SanctionController';

import { RegistrableSubscriberService } from "../eventbus/cloud/RegistrableSubscriberService";
import { DepositServiceHandler } from  "../eventbus/cloud/service/DepositServiceHandler";

import { RegistrableEntityListener } from "../eventbus/firestore/RegistrableEntityListener";
import { UserListener } from "../eventbus/firestore/entityListeners/UserListener";
import { RemittanceListener } from "../eventbus/firestore/entityListeners/RemittanceListener";
import { DepositListener } from "../eventbus/firestore/entityListeners/DepositListener";
import { CustomerListener } from "../eventbus/firestore/entityListeners/CustomerListener";



const container = new Container();
/* Repos */

container.bind<UserRepository>(TYPES.UserRepository).to(UserRepositoryImpl);
container.bind<RemittanceRepository>(TYPES.RemittanceRepository).to(RemittanceRepositoryImpl);
container.bind<CustomerRepository>(TYPES.CustomerRepository).to(CustomerRepositoryImpl);
container.bind<DefaultRepository>(TYPES.DefaultRepository).to(DefaultRepositoryImpl);
container.bind<EventRepository>(TYPES.EventRepository).to(EventRepositoryImpl);
container.bind<RoleRepository>(TYPES.RoleRepository).to(RoleRepositoryImpl);
container.bind<DepositRepository>(TYPES.DepositRepository).to(DepositRepositoryImpl);
container.bind<SanctionRepository>(TYPES.SanctionRepository).to(SanctionRepositoryImpl);
/* Services */
container.bind<UserService>(TYPES.UserService).to(UserServiceImpl);
container.bind<RemittanceService>(TYPES.RemittanceService).to(RemittanceServiceImpl);
container.bind<CustomerService>(TYPES.CustomerService).to(CustomerServiceImpl);
container.bind<DefaultService>(TYPES.DefaultService).to(DefaultServiceImpl);
container.bind<EventService>(TYPES.EventService).to(EventServiceImpl);
container.bind<RoleService>(TYPES.RoleService).to(RoleServiceImpl);
container.bind<DepositService>(TYPES.DepositService).to(DepositServiceImpl);
container.bind<SanctionService>(TYPES.SanctionService).to(SanctionServiceImpl);

/* service handlers */
container.bind<RegistrableSubscriberService>(TYPES.ServiceHandler).to(DepositServiceHandler);

/* entity listners */
container.bind<RegistrableEntityListener>(TYPES.EntityListener).to(UserListener);
container.bind<RegistrableEntityListener>(TYPES.EntityListener).to(RemittanceListener);
container.bind<RegistrableEntityListener>(TYPES.EntityListener).to(DepositListener);
container.bind<RegistrableEntityListener>(TYPES.EntityListener).to(CustomerListener);

/* Controllers */
container.bind<RegistrableController>(TYPES.ControllerV1).to(UserController);
container.bind<RegistrableController>(TYPES.ControllerV1).to(CustomerController);
container.bind<RegistrableController>(TYPES.ControllerV1).to(RemittanceController);
container.bind<RegistrableController>(TYPES.ControllerV1).to(RoleController);
container.bind<RegistrableController>(TYPES.ControllerV1).to(DepositController);
container.bind<RegistrableController>(TYPES.ControllerV1).to(EventController);
container.bind<RegistrableController>(TYPES.ControllerV1).to(SanctionController);


export default container;