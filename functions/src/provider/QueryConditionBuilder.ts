import { Errors } from "typescript-rest";
import DocumentSnapshot = FirebaseFirestore.DocumentSnapshot;

export interface Filter {
    lhs: string;
    whereOp: string;
    rhs: string;
}

export type SortOrder = 'asc' | 'desc';

export class QueryCondition {
    filters: Filter[];
    limit: number;
    orderBy: string;
    sortOrder: SortOrder;
    page:number;
    startAfter: DocumentSnapshot | string

    constructor() {
        this.filters = [];
    }

    public addFilter(fieldName: string, fieldValue: string): QueryCondition {
        if (fieldValue != null && fieldName != null) {
            this.filters.push({
                lhs: fieldName,
                whereOp: '==',
                rhs: fieldValue
            })
        }
        return this;
    }

    public withLimit(limit: number): QueryCondition {
        if (limit != null) {
            if (limit <= 0)
                throw new Errors.BadRequestError('Limit needs to be greater than 0')
            this.limit = limit;
        }
        return this;
    }

    public withOrderBy(fieldName: string): QueryCondition {
        if (fieldName != null)
            this.orderBy = fieldName;
        return this;
    }

    public withSortOrder(sortOrder: SortOrder): QueryCondition {
        if (sortOrder != null)
            this.sortOrder = sortOrder;
        return this;

    }

    public withPage(page:number): QueryCondition {
        if (page != null) {
            if (page <= 0)
                throw new Errors.BadRequestError('Page needs to be greater than 0')
            this.page = page;
        }
        return this;
    }

    public withStartAfter(field?: string, doc?: DocumentSnapshot) {
        if (field != null)
            this.startAfter = field;
        else if (doc != null)
            this.startAfter = doc;
        return this;
    }

    public toQueryString(): string {
        let val = "";
        for (let i = 0; i < this.filters.length; i++) {
            if (i === 0)
                val += '?';
            else
                val += '&'

            val += this.filters[i].lhs + '=' + this.filters[i].rhs
        }
        if (this.orderBy != null) {
            if (val.length > 0)
                val += '&order_by' + '=' + this.orderBy;
            else
                val += '?order_by' + '=' + this.orderBy;
        }
        if (this.sortOrder != null) {
            if (val.length > 0)
                val += '&sort_order' + '=' + this.sortOrder;
            else
                val += '?sort_order' + '=' + this.sortOrder;
        }
        if (this.page != null) {
            if (val.length > 0)
                val += '&page' + '=' + this.page;
            else
                val += '?page' + '=' + this.page;
        }
        if (this.limit != null) {
            if (val.length > 0)
                val += '&limit' + '=' + this.limit;
            else
                val += '?limit' + '=' + this.limit;
        }
        return val;
    }
}