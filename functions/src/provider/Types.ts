const TYPES = {
    UserRepository: Symbol('UserRepository'),
    EventRepository: Symbol('EventRepository'),
    CustomerRepository: Symbol('CustomerRepository'),
    RemittanceRepository: Symbol('RemittanceRepository'),
    DefaultRepository: Symbol('DefaultRepository'),
    RoleRepository: Symbol('RoleRepository'),
    DepositRepository: Symbol('RoleRepository'),
    SanctionRepository: Symbol('SanctionRepository'),

    UserService: Symbol('UserService'),
    EventService: Symbol('EventService'),
    CustomerService: Symbol('CustomerService'),
    RemittanceService: Symbol('RemittanceService'),
    DefaultService: Symbol('DefaultService'),
    RoleService: Symbol('RoleService'),
    DepositService: Symbol('RoleService'),
    SanctionService: Symbol('SanctionService'),

    ServiceHandler: Symbol('ServiceHandler'),
    EntityListener: Symbol('EntityListener'),

    ControllerV1: Symbol('ControllerV1'),
    ControllerV2: Symbol('ControllerV2')
};

export default TYPES;