export interface RegistrableController {

    getById(id: string): Promise<void>;

    update(id: string, body: any): Promise<void>;

    get(): Promise<void>;

    delete(id: string): Promise<void>;

    create(body: any): Promise<any>;
}