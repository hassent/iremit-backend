import * as Hal from 'hal';
import {HATEOAS_API_URL} from '../../provider/Global';

const version = '/v1';


export class Self {
    static Base = `${HATEOAS_API_URL}${version}`;
    static User = `${HATEOAS_API_URL}${version}/users`;
    static Customer = `${HATEOAS_API_URL}${version}/customers`;
    static Default = `${HATEOAS_API_URL}${version}/defaults`;
    static Remittance = `${HATEOAS_API_URL}${version}/remittances`;
    static Event = `${HATEOAS_API_URL}${version}/events`;
    static Role = `${HATEOAS_API_URL}${version}/roles`;
    static Deposit = `${HATEOAS_API_URL}${version}/deposits`;
    static Sanction = `${HATEOAS_API_URL}${version}/UN-Sanction-List`;



}

export class Builder {
    private resource;

    constructor(selfLink: string, id: string, body?: any) {
        if (id && id != null)
            this.resource = new Hal.Resource(body, `${selfLink}/${id}`)
        else
            this.resource = new Hal.Resource(body, selfLink)
    }

    private rootLink(name: string, link: string, id: string, queryString?: string) {
        if (id != null)
            this.resource.link(name, link + `/${id}`);
        else
            this.resource.link(name, link + (queryString ? queryString : ''))
    }

    withUserLink(id: string, queryString?: string): Builder {
        this.rootLink('users', Self.User, id, queryString);
        return this;
    }







    withEmbedded(name: string, resources: Hal[]) {
        this.resource.embed(name, resources)
        return this;
    }

    build(): Hal {
        return this.resource;
    }
}

