import { RegistrableController } from '../../RegisterableController';
import { inject, injectable } from "inversify";
import TYPES from '../../../provider/Types';
import { DepositService } from '../../../service/DepositService';
import { Builder } from "../../../provider/validator";
import * as Resource from "../ResourceBuilder";
import { Deposit, DepositStatus } from '../../../model/Deposit'


import { Path, PathParam, POST, GET, PUT, DELETE, Errors, Return, Context, ServiceContext, PATCH, QueryParam } from "typescript-rest";
import { Tags, Produces, Response } from 'typescript-rest-swagger';
import container from '../../../provider/inversify.config';
import { CustomerService } from '../../../service/CustomerService';
import { Customer } from '../../../model/Customer';
import { UserService } from '../../../service/UserService';
import { SortOrder, QueryCondition } from '../../../provider/QueryConditionBuilder';

/**
 * Deposit resource controller class which exposes the Deposit apis
 *
 * @export
 * @class DepositController
 * @implements {RegistrableController}
 */
@Path("/v1/deposits")
@Tags('Deposit Controller')
@Produces('application/json')
@injectable()
export class DepositController implements RegistrableController {

    @inject(TYPES.DepositService)
    private depositService: DepositService =
        container.get<DepositService>(TYPES.DepositService);


    /**
     * Creates a new Deposit resource
     *
     * @param {Deposit} body a json serialized Deposit object as a request body.
     * @returns {Promise<any>}
     * @memberof DepositController
     */
    @Response<Errors.BadRequestError>(400)
    @Response<Return.NewResource<Deposit>>(201, "Created")
    @POST

    async create(body: Deposit): Promise<any> {
        let deposit: Deposit;
        try {
            deposit = Builder<Deposit>()
                .users(body.users)
                .value(body.value)
                .status(DepositStatus.NEW)
                .build()

        } catch (error) {
            console.error(error);
            return Promise.reject(Errors.BadRequestError);
        }

        try {
            const createdDeposit = await this.depositService.createDeposit(deposit);
            return new Return.NewResource<Deposit>(`${Resource.Self.Deposit}/${createdDeposit.id}`);
        } catch (error) {
            console.error(error);
            return Promise.reject(error);
        }
    }
    /**
     * This endpoint deletes a Deposit resource specified by the id in the path.
     * It is not implemented by the server yet and a NotImplementedError will be thrown as a response.
     *
     * @param {string} id firebase authentication Deposit id: uid
     * @returns {Promise<void>}
     * @memberof DepositController
     */
    @Response<Errors.NotImplementedError>(501)
    @DELETE
    @Path(":id")
    async delete(@PathParam('id') id: string): Promise<void> {
        return Promise.reject(new Errors.NotImplementedError());
    }

    /**
     * It lists all the available Deposits. Optional query parameters can be included to search for a specific resource.
     * In case there is no resource in the Deposit collection, a NotFoundError might be thrown
     *
     * @returns {Promise<void>}
     * @memberof DepositController
     */
    @Response<Errors.NotFoundError>(404)
    @Response<Array<Deposit>>(200)
    @GET
    public async get(@QueryParam("limit") limit?: number,
        @QueryParam("order_by") orderBy?: string,
        @QueryParam("sort_order") sortOrder?: SortOrder,
        @QueryParam("page") page?: number): Promise<void> {
        try {
            const condition = new QueryCondition()
                .withOrderBy(orderBy)
                .withSortOrder(sortOrder)
                .withPage(page)
                .withLimit(limit);
            const deposits = await this.depositService.getDeposits(condition);
            if (deposits.length > 0) {
                const response = new Resource.Builder(Resource.Self.Deposit + condition.toQueryString(), null, {
                    deposits: deposits.map(deposit => {
                        return new Resource.Builder(Resource.Self.Deposit,
                            deposit.id, { id: deposit.id, status: deposit.status }
                        ).build();
                    })
                }).build();
                return response.toJSON();
            } else {
                throw new Errors.NotFoundError();
            }
        } catch (error) {
            console.log(error);
            return Promise.reject(error);
        }
    }

    /**
     * This endpoint returns a specific Deposit resource specified by the id in the path.
     * If no Deposit is found, a NotFoundError will be thrown.
     *
     * @param {string} id firebase authentication Deposit id: uid
     * @returns {Promise<void>}
     * @memberof DepositController
     */
    @Response<Errors.NotFoundError>(404)
    @Response<Deposit>(200)
    @GET
    @Path(":id")
    async getById(@PathParam('id') id: string): Promise<void> {
        try {
            const deposit = await this.depositService.getDepositById(id);
            if (deposit) {
                const response = new Resource.Builder(Resource.Self.Deposit, deposit.id, deposit)
                    .build();
                return response.toJSON();
            } else {
                throw new Errors.NotFoundError();
            }
        } catch (error) {
            console.log(error);
            return Promise.reject(error);
        }
    }

    /**
     * This endpoint updates a Deposit resource from the modified data in the request body.
     * If the request body is not a typeOf Deposit, a BadRequestError might be thrown. In case
     * a Deposit cannot be found by the specified id in the request path, a NotFoundError will be thrown.
     *
     * @param {string} id firebase authentication Deposit id: uid
     * @param {Deposit} body updated Deposit object as a json serialized request body
     * @returns {Promise<void>}
     * @memberof DepositController
     */
    @Response<Errors.BadRequestError>(400)
    @Response<Errors.NotFoundError>(404)
    @Response<Deposit>(200)
    @Path(":id")
    @PUT
    async update(@PathParam('id') id: string, body: Deposit): Promise<void> {
        const depositUpdate: Deposit = body;
        if (!depositUpdate) {
            return Promise.reject(new Errors.BadRequestError());
        }
        console.log(depositUpdate);
        try {
            const updatedDeposit = await this.depositService.updateDeposit(id, depositUpdate);
            const response = new Resource.Builder(Resource.Self.Deposit, updatedDeposit.id).build();
            return response.toJSON();
        } catch (error) {
            return Promise.reject(error);
        }
    }


}

