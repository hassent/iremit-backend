import { RegistrableController } from '../../RegisterableController';
import { inject, injectable } from "inversify";
import TYPES from '../../../provider/Types';
import { UserService } from '../../../service/UserService';
import { Builder } from "../../../provider/validator";
import * as Resource from "../ResourceBuilder";
import { User, UserStatus } from "../../../model/User";


import { Path, PathParam, POST, GET, PUT, DELETE, Errors, Return, Context, ServiceContext, PATCH, QueryParam } from "typescript-rest";
import { Tags, Produces, Response } from 'typescript-rest-swagger';
import container from '../../../provider/inversify.config';
import { SortOrder, QueryCondition } from '../../../provider/QueryConditionBuilder';

/**
 * User resource controller class which exposes the user apis
 *
 * @export
 * @class UserController
 * @implements {RegistrableController}
 */
@Path("/v1/users")
@Tags('User Controller')
@Produces('application/json')
@injectable()
export class UserController implements RegistrableController {

    @inject(TYPES.UserService)
    private userService: UserService = container.get<UserService>(TYPES.UserService);

    /**
     * Creates a new user resource
     *
     * @param {User} body a json serialized user object as a request body.
     * @returns {Promise<any>}
     * @memberof UserController
     */
    @Response<Errors.NotImplementedError>(501)
    @POST

    async create(body: User): Promise<any> {
        let user: User;
        return Promise.reject(new Errors.NotImplementedError());
    }
    /**
     * This endpoint deletes a user resource specified by the id in the path.
     * It is not implemented by the server yet and a NotImplementedError will be thrown as a response.
     *
     * @param {string} id firebase authentication user id: uid
     * @returns {Promise<void>}
     * @memberof UserController
     */
    @Response<Errors.NotImplementedError>(501)
    @DELETE
    @Path(":id")
    async delete(@PathParam('id') id: string): Promise<void> {
        return Promise.reject(new Errors.NotImplementedError());
    }

    /**
     * It lists all the available users. Optional query parameters can be included to search for a specific resource.
     * In case there is no resource in the user collection, a NotFoundError might be thrown
     *
     * @returns {Promise<void>}
     * @memberof UserController
     */
    @Response<Errors.NotFoundError>(404)
    @Response<Array<User>>(200)
    @GET
    public async get(@QueryParam("limit") limit?: number,
        @QueryParam("order_by") orderBy?: string,
        @QueryParam("sort_order") sortOrder?: SortOrder,
        @QueryParam("page") page?: number): Promise<void> {
        try {
            const condition = new QueryCondition()
                .withOrderBy(orderBy)
                .withSortOrder(sortOrder)
                .withPage(page)
                .withLimit(limit);
            const users = await this.userService.getUsers(condition);
            if (users.length > 0) {
                const response = new Resource.Builder(Resource.Self.User + condition.toQueryString(), null, {
                    users: users.map(user => {
                        return new Resource.Builder(Resource.Self.User,
                            user.id, { id: user.id, msisdn: user.referrerId, mail: user.email }
                        ).build();
                    })
                }).build();

                return response.toJSON();
            } else {
                throw new Errors.NotFoundError();
            }
        } catch (error) {
            console.log(error);
            return Promise.reject(error);
        }
    }

    /**
     * This endpoint returns a specific user resource specified by the id in the path.
     * If no user is found, a NotFoundError will be thrown.
     *
     * @param {string} id firebase authentication user id: uid
     * @returns {Promise<void>}
     * @memberof UserController
     */
    @Response<Errors.NotFoundError>(404)
    @Response<User>(200)
    @GET
    @Path(":id")
    async getById(@PathParam('id') id: string): Promise<void> {
        try {
            const user = await this.userService.getUserById(id);
            if (user) {
                const response = new Resource.Builder(Resource.Self.User, user.id, user)
                    .build();
                return response.toJSON();
            } else {
                throw new Errors.NotFoundError();
            }
        } catch (error) {
            console.log(error);
            return Promise.reject(error);
        }
    }

    /**
     * This endpoint updates a user resource from the modified data in the request body.
     * If the request body is not a typeOf user, a BadRequestError might be thrown. In case
     * a user cannot be found by the specified id in the request path, a NotFoundError will be thrown.
     *
     * @param {string} id firebase authentication user id: uid
     * @param {User} body updated user object as a json serialized request body
     * @returns {Promise<void>}
     * @memberof UserController
     */
    @Response<Errors.BadRequestError>(400)
    @Response<Errors.NotFoundError>(404)
    @Response<User>(200)
    @Path(":id")
    @PUT
    async update(@PathParam('id') id: string, body: User): Promise<void> {
        const userUpdate: User = body;
        if (!userUpdate) {
            return Promise.reject(new Errors.BadRequestError);
        }
        console.log(userUpdate);
        try {
            const updatedUser = await this.userService.updateUser(id, userUpdate);
            const response = new Resource.Builder(Resource.Self.User, updatedUser.id).build();
            return response.toJSON();
        } catch (error) {
            return Promise.reject(error);
        }
    }

    /**
     * This endpoint updates a user resource from the modified data in the request body.
     * If the request body is not a typeOf user, a BadRequestError might be thrown. In case
     * a user cannot be found by the specified id in the request path, a NotFoundError will be thrown.
     *
     * @param {string} id firebase authentication user id: uid
     * @param {User} body updated user object as a json serialized request body
     * @returns {Promise<void>}
     * @memberof UserController
     */
    @Response<Errors.BadRequestError>(400)
    @Response<Errors.NotFoundError>(404)
    @Response<User>(200)
    @Path(":id")
    @PATCH
    async updateStatus(@PathParam('id') id: string, status: UserStatus): Promise<void> {
        const updatedStatus: UserStatus = status;
        if (!updatedStatus) {
            return Promise.reject(new Errors.BadRequestError);
        }
        console.log(updatedStatus);
        try {
            const updatedUser = await this.userService.updateUserStatus(id, updatedStatus);
            const response = new Resource.Builder(Resource.Self.User, updatedUser.id).build();
            return response.toJSON();
        } catch (error) {
            return Promise.reject(error);
        }
    }

}

