import { RegistrableController } from '../../RegisterableController';
import { inject, injectable } from "inversify";
import InversifyTypes from '../../../provider/Types';
import { CustomerService } from '../../../service/CustomerService';
import { Builder } from "../../../provider/validator";
import * as Resource from "../ResourceBuilder";
import { Customer } from "../../../model/Customer";


import { Path, PathParam, POST, GET, PUT, DELETE, Errors, Return, Context, QueryParam, ServiceContext, PATCH } from "typescript-rest";
import { Tags, Produces, Response } from 'typescript-rest-swagger';
import container from '../../../provider/inversify.config';
import { SortOrder, QueryCondition } from '../../../provider/QueryConditionBuilder';

/**
 * Customer resource controller class which exposes the customer apis
 *
 * @export
 * @class CustomerController
 * @implements {RegistrableController}
 */
@Path("/v1/customers")
@Tags('Customer Controller')
@Produces('application/json')
@injectable()
export class CustomerController implements RegistrableController {

    @inject(InversifyTypes.CustomerService)
    private customerService: CustomerService = container.get<CustomerService>(InversifyTypes.CustomerService);

    /**
     * Creates a new customer resource
     *
     * @param {Customer} body a json serialized customer object as a request body.
     * @returns {Promise<any>}
     * @memberof CustomerController
     */
    @Response<Errors.NotImplementedError>(501)
    @POST
    async create(body: Customer): Promise<any> {
        let customer: Customer;
        return Promise.reject(new Errors.NotImplementedError());
    }
    /**
     * This endpoint deletes a customer resource specified by the id in the path.
     * It is not implemented by the server yet and a NotImplementedError will be thrown as a response.
     *
     * @param {string} id firebase authentication customer id: uid
     * @returns {Promise<void>}
     * @memberof CustomerController
     */
    @Response<Errors.NotImplementedError>(501)
    @DELETE
    @Path(":id")
    async delete(@PathParam('id') id: string): Promise<void> {
        return Promise.reject(new Errors.NotImplementedError());
    }

    /**
     * It lists all the available customers. Optional query parameters can be included to search for a specific resource.
     * In case there is no resource in the customer collection, a NotFoundError might be thrown
     *
     * @returns {Promise<void>}
     * @memberof CustomerController
     */
    @Response<Errors.NotFoundError>(404)
    @Response<Array<Customer>>(200)
    @GET
    public async get(@QueryParam("limit") limit?: number,
        @QueryParam("order_by") orderBy?: string,
        @QueryParam("sort_order") sortOrder?: SortOrder,
        @QueryParam("page") page?: number): Promise<void> {
        try {
            const condition = new QueryCondition()
                .withOrderBy(orderBy)
                .withSortOrder(sortOrder)
                .withPage(page)
                .withLimit(limit);
            const customers = await this.customerService.getCustomers(condition);
            if (customers.length > 0) {
                const response = new Resource.Builder(Resource.Self.Customer + condition.toQueryString(), null, {
                    customers: customers.map(customer => {
                        return new Resource.Builder(Resource.Self.Customer,
                            customer.id, { type: customer.type, msisdn: customer.msisdn, name: customer.name }
                        ).build();
                    })
                }).build();
                return response.toJSON();
            } else {
                throw new Errors.NotFoundError();
            }
        } catch (error) {
            console.log(error);
            return Promise.reject(error);
        }
    }

    /**
     * This endpoint returns a specific customer resource specified by the id in the path.
     * If no customer is found, a NotFoundError will be thrown.
     *
     * @param {string} id firebase authentication customer id: uid
     * @returns {Promise<void>}
     * @memberof CustomerController
     */
    @Response<Errors.NotFoundError>(404)
    @Response<Customer>(200)
    @GET
    @Path(":id")
    async getById(@PathParam('id') id: string): Promise<void> {
        try {
            const customer = await this.customerService.getCustomerById(id);
            if (customer) {
                const response = new Resource.Builder(Resource.Self.Customer, customer.id, customer)
                    .build();
                return response.toJSON();
            } else {
                throw new Errors.NotFoundError();
            }
        } catch (error) {
            console.log(error);
            return Promise.reject(error);
        }
    }

    /**
     * This endpoint updates a customer resource from the modified data in the request body.
     * If the request body is not a typeOf customer, a BadRequestError might be thrown. In case
     * a customer cannot be found by the specified id in the request path, a NotFoundError will be thrown.
     *
     * @param {string} id firebase authentication customer id: uid
     * @param {Customer} body updated customer object as a json serialized request body
     * @returns {Promise<void>}
     * @memberof CustomerController
     */
    @Response<Errors.BadRequestError>(400)
    @Response<Errors.NotFoundError>(404)
    @Response<Customer>(200)
    @Path(":id")
    @PUT
    async update(@PathParam('id') id: string, body: Customer): Promise<void> {
        const customerUpdate: Customer = body;
        if (!customerUpdate) {
            return Promise.reject(new Errors.BadRequestError);
        }
        console.log(customerUpdate);
        try {
            const updatedCustomer = await this.customerService.updateCustomer(id, customerUpdate);
            const response = new Resource.Builder(Resource.Self.Customer, updatedCustomer.id).build();
            return response.toJSON();
        } catch (error) {
            return Promise.reject(error);
        }
    }

}

