import { RegistrableController } from '../../RegisterableController';
import { inject, injectable } from "inversify";
import TYPES from '../../../provider/Types';
import { RoleService } from '../../../service/RoleService';
import { Builder } from "../../../provider/validator";
import * as Resource from "../ResourceBuilder";
import { Role, Permission } from "../../../model/Role";


import { Path, PathParam, POST, GET, PUT, DELETE, Errors, Return, Context, ServiceContext, PATCH, QueryParam } from "typescript-rest";
import { Tags, Produces, Response } from 'typescript-rest-swagger';
import container from '../../../provider/inversify.config';
import { SortOrder, QueryCondition } from '../../../provider/QueryConditionBuilder';

/**
 * Role resource controller class which exposes the role apis
 *
 * @export
 * @class RoleController
 * @implements {RegistrableController}
 */
@Path("/v1/roles")
@Tags('Role Controller')
@Produces('application/json')
@injectable()
export class RoleController implements RegistrableController {

    @inject(TYPES.RoleService)
    private roleService: RoleService = container.get<RoleService>(TYPES.RoleService);

    /**
     * Creates a new role resource
     *
     * @param {Role} body a json serialized role object as a request body.
     * @returns {Promise<any>}
     * @memberof RoleController
     */
    @Response<Errors.BadRequestError>(400)
    @Response<Return.NewResource<Role>>(201, "Created")
    @POST

    async create(body: Role): Promise<any> {
        let role: Role;
        try {
            role = Builder<Role>()
                .id(body.id)
                .email(body.email)
                .assignee(body.assignee)
                .permissions(body.permissions)
                .build()
        } catch (error) {
            console.error(error);
            return Promise.reject(new Errors.BadRequestError());
        }
        console.log(role);
        try {
            const createdRole = await this.roleService.createRole(role);
            return new Return.NewResource<Role>(`${Resource.Self.Remittance}/${createdRole.id}`);
        } catch (error) {
            console.error(error);
            return Promise.reject(error);
        }
    }
    /**
     * This endpoint deletes a role resource specified by the id in the path.
     * It is not implemented by the server yet and a NotImplementedError will be thrown as a response.
     *
     * @param {string} id firebase authentication user id: uid
     * @returns {Promise<void>}
     * @memberof RoleController
     */
    @Response<Errors.NotImplementedError>(501)
    @DELETE
    @Path(":id")
    async delete(@PathParam('id') id: string): Promise<void> {
        return Promise.reject(new Errors.NotImplementedError());
    }

    /**
     * It lists all the available roles. Optional query parameters can be included to search for a specific resource.
     * In case there is no resource in the role collection, a NotFoundError might be thrown
     *
     * @returns {Promise<void>}
     * @memberof RoleController
     */
    @Response<Errors.NotFoundError>(404)
    @Response<Array<Role>>(200)
    @GET
    public async get(@QueryParam("limit") limit?: number,
        @QueryParam("order_by") orderBy?: string,
        @QueryParam("sort_order") sortOrder?: SortOrder,
        @QueryParam("page") page?: number): Promise<void> {
        try {
            const condition = new QueryCondition()
                .withOrderBy(orderBy)
                .withSortOrder(sortOrder)
                .withPage(page)
                .withLimit(limit);
            const roles = await this.roleService.getRoles(condition);
            if (roles.length > 0) {
                const response = new Resource.Builder(Resource.Self.Role + condition.toQueryString(), null, {
                    roles: roles.map(role => {
                        return new Resource.Builder(Resource.Self.Role,
                            role.id,
                            { email: role.email, assignee: role.assignee }
                        ).build();
                    })
                })
                    .build();
                return response.toJSON();
            } else {
                throw new Errors.NotFoundError();
            }
        } catch (error) {
            console.log(error);
            return Promise.reject(error);
        }
    }

    /**
     * This endpoint returns a specific role resource specified by the id in the path.
     * If no role is found, a NotFoundError will be thrown.
     *
     * @param {string} id firebase authentication user id: uid
     * @returns {Promise<void>}
     * @memberof RoleController
     */
    @Response<Errors.NotFoundError>(404)
    @Response<Role>(200)
    @GET
    @Path(":id")
    async getById(@PathParam('id') id: string): Promise<void> {
        try {
            const role = await this.roleService.getRoleById(id);
            if (role) {
                const response = new Resource.Builder(Resource.Self.Role, role.id, role)
                    .build();
                return response.toJSON();
            } else {
                throw new Errors.NotFoundError();
            }
        } catch (error) {
            console.log(error);
            return Promise.reject(error);
        }
    }

    /**
     * This endpoint updates a role resource from the modified data in the request body.
     * If the request body is not a typeOf role, a BadRequestError might be thrown. In case
     * a role cannot be found by the specified id in the request path, a NotFoundError will be thrown.
     *
     * @param {string} id firebase authentication user id: uid
     * @param {Role} body updated role object as a json serialized request body
     * @returns {Promise<void>}
     * @memberof RoleController
     */
    @Response<Errors.BadRequestError>(400)
    @Response<Errors.NotFoundError>(404)
    @Response<Role>(200)
    @Path(":id")
    @PATCH
    async update(@PathParam('id') id: string, permissions: Array<Permission>): Promise<void> {
        const updatedPermissions: Array<Permission> = permissions;
        if (!updatedPermissions) {
            return Promise.reject(new Errors.BadRequestError);
        }
        console.log(updatedPermissions);
        try {
            const updatedRole = await this.roleService.updateRole(id, updatedPermissions);
            const response = new Resource.Builder(Resource.Self.Role, updatedRole.id).build();
            return response.toJSON();
        } catch (error) {
            return Promise.reject(error);
        }
    }



}

