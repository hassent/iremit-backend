import { RegistrableController } from '../../RegisterableController';
import { inject, injectable } from "inversify";
import TYPES from '../../../provider/Types';
import { EventService } from "../../../service/EventService";
import { Path, PathParam, POST, GET, PUT, DELETE, Errors, Return, Context, ServiceContext, PATCH, QueryParam } from "typescript-rest";
import { Tags, Produces, Response } from 'typescript-rest-swagger';
import { Event } from '../../../model/Event';
import * as Resource from "../ResourceBuilder";
import container from '../../../provider/inversify.config';
import { SortOrder, QueryCondition } from '../../../provider/QueryConditionBuilder';

/**
 * Event resource controller class which exposes the event apis
 *
 * @export
 * @class EventController
 */
@Path("/v1/events")
@Tags('Event Controller')
@Produces('application/json')
@injectable()
export class EventController implements RegistrableController {


    @inject(TYPES.EventService)
    private eventService: EventService;


    /**
     * Creates a new event resource
     * This endpoint creates an event resource specified by the id in the path.
     * It is not implemented by the server yet and a NotImplementedError will be thrown as a response.
     *
     * @param {Event} body
     * @returns {Promise<any>}
     * @memberof EventController
     */
    @Response<Errors.NotImplementedError>(501)
    @POST
    async create(body: Event): Promise<any> {
        return Promise.reject(new Errors.NotImplementedError());
    }

    /**
     * This endpoint deletes an event resource specified by the id in the path.
     * It is not implemented by the server yet and a NotImplementedError will be thrown as a response.
     *
     * @param {string} id
     * @returns {Promise<void>}
     * @memberof EventController
     */
    @Response<Errors.NotImplementedError>(501)
    @DELETE
    @Path(":id")
    async delete(@PathParam('id') id: string): Promise<void> {
        return Promise.reject(new Errors.NotImplementedError());
    }

    /**
     * It lists all the available events. Optional query parameters can be included to search for a specific resource.
     * In case there is no resource in the user collection, a NotFoundError might be thrown
     *
     * @param {ServiceContext} [context]
     * @returns {Promise<void>}
     * @memberof EventController
     */
    @Response<Errors.NotFoundError>(404)
    @Response<Array<Event>>(200)
    @GET
    public async get(@QueryParam("limit") limit?: number,
        @QueryParam("order_by") orderBy?: string,
        @QueryParam("sort_order") sortOrder?: SortOrder,
        @QueryParam("page") page?: number): Promise<void> {
        try {
            const condition = new QueryCondition()
                .withOrderBy(orderBy)
                .withSortOrder(sortOrder)
                .withPage(page)
                .withLimit(limit);

            const events = await this.eventService.getEvents(condition);
            if (events.length > 0) {
                const response = new Resource.Builder(Resource.Self.Event + condition.toQueryString(), null, {
                    events: events.map(event => {
                        return new Resource.Builder(Resource.Self.Event,
                            event.id,
                            { eventName: event.name, eventId: event.id, eventType: event.type }
                        ).build();
                    })
                }).build();

                return response.toJSON();
            } else {
                throw new Errors.NotFoundError();
            }
        } catch (error) {
            return Promise.reject(error);
        }
    }

    /**
     * This endpoint returns a specific adcompletion resource specified by the id in the path.
     * If no resource is found, a NotFoundError will be thrown.
     * It is not implemented by the server yet and a NotImplementedError will be thrown as a response.
     *
     * @param {string} id
     * @returns {Promise<void>}
     * @memberof EventController
     */
    @Response<Errors.NotImplementedError>(501)
    @Path(":id")
    @GET
    async getById(@PathParam('id') id: string): Promise<void> {
        return Promise.reject(new Errors.NotImplementedError());
    }

    /**
     * This endpoint updates an event resource from the modified data in the request body.
     * It is not implemented by the server yet and a NotImplementedError will be thrown as a response.
     *
     * @param {string} id
     * @returns {Promise<void>}
     * @memberof EventController
     */
    @Response<Errors.NotImplementedError>(501)
    @Path(":id")
    @PUT
    async update(@PathParam('id') id: string): Promise<void> {
        return Promise.reject(new Errors.NotImplementedError());
    }
}