import { RegistrableController } from '../../RegisterableController';
import { inject, injectable } from "inversify";
import TYPES from '../../../provider/Types';
import { SanctionService } from '../../../service/SanctionService';
import { Builder } from "../../../provider/validator";
import * as Resource from "../ResourceBuilder";
import { Sanction } from "../../../model/Sanction";

import { Path, PathParam, POST, GET, PUT, DELETE, Errors, Return, Context, QueryParam, ServiceContext, PATCH } from "typescript-rest";
import { Tags, Produces, Response } from 'typescript-rest-swagger';
import container from '../../../provider/inversify.config';
import { QueryCondition, SortOrder } from '../../../provider/QueryConditionBuilder';

/**
 * Sanction resource controller class which exposes the sanction apis
 *
 * @export
 * @class SanctionController
 * @implements {RegistrableController}
 */
@Path("/v1/sanctions")
@Tags('Sanction Controller')
@Produces('application/json')
@injectable()
export class SanctionController implements RegistrableController {

    @inject(TYPES.SanctionService)
    private sanctionService: SanctionService = container.get<SanctionService>(TYPES.SanctionService);

    /**
     * Creates a new sanction resource
     *
     * @param {Sanction} body a json serialized sanction object as a request body.
     * @returns {Promise<any>}
     * @memberof SanctionController
     */
    @Response<Errors.NotImplementedError>(501)
    @POST
    async create(body: Sanction): Promise<any> {
        let sanction: Sanction;
        return Promise.reject(new Errors.NotImplementedError());
    }
    /**
     * This endpoint deletes a sanction resource specified by the id in the path.
     * It is not implemented by the server yet and a NotImplementedError will be thrown as a response.
     *
     * @param {string} id firebase authentication sanction id: uid
     * @returns {Promise<void>}
     * @memberof SanctionController
     */
    @Response<Errors.NotImplementedError>(501)
    @DELETE
    @Path(":id")
    async delete(@PathParam('id') id: string): Promise<void> {
        return Promise.reject(new Errors.NotImplementedError());
    }

    /**
     * It lists all the available sanctions. Optional query parameters can be included to search for a specific resource.
     * In case there is no resource in the sanction collection, a NotFoundError might be thrown
     *
     * @returns {Promise<void>}
     * @memberof SanctionController
     */
    @Response<Errors.NotFoundError>(404)
    @Response<Array<Sanction>>(200)
    @GET
    public async get(@QueryParam("limit") limit?: number,
        @QueryParam("order_by") orderBy?: string,
        @QueryParam("sort_order") sortOrder?: SortOrder,
        @QueryParam("page") page?: number): Promise<void> {
        try {
            const condition = new QueryCondition()
                .withOrderBy(orderBy)
                .withSortOrder(sortOrder)
                .withPage(page)
                .withLimit(limit);
            const sanctions = await this.sanctionService.getSanctions(condition);
            if (sanctions.length > 0) {
                const response = new Resource.Builder(Resource.Self.Sanction + condition.toQueryString(), null, {
                    sanctions: sanctions.map(sanction => {
                        return new Resource.Builder(Resource.Self.Sanction,
                            sanction.id, { id: sanction.id, fullName: `${sanction.firstName} ${sanction.middleName} ${sanction.lastName}`, country: sanction.country }
                        ).build();
                    })
                }).build();
                return response.toJSON();
            } else {
                throw new Errors.NotFoundError();
            }
        } catch (error) {
            console.log(error);
            return Promise.reject(error);
        }
    }

    /**
     * This endpoint returns a specific sanction resource specified by the id in the path.
     * If no sanction is found, a NotFoundError will be thrown.
     *
     * @param {string} id firebase authentication sanction id: uid
     * @returns {Promise<void>}
     * @memberof SanctionController
     */
    @Response<Errors.NotFoundError>(404)
    @Response<Sanction>(200)
    @GET
    @Path(":id")
    async getById(@PathParam('id') id: string): Promise<void> {
        try {
            const sanction = await this.sanctionService.getSanctionById(id);
            if (sanction) {
                const response = new Resource.Builder(Resource.Self.Sanction, sanction.id, sanction)
                    .build();
                return response.toJSON();
            } else {
                throw new Errors.NotFoundError();
            }
        } catch (error) {
            console.log(error);
            return Promise.reject(error);
        }
    }

    /**
     * This endpoint updates a sanction resource from the modified data in the request body.
     * If the request body is not a typeOf sanction, a BadRequestError might be thrown. In case
     * a sanction cannot be found by the specified id in the request path, a NotFoundError will be thrown.
     *
     * @param {string} id firebase authentication sanction id: uid
     * @param {Sanction} body updated sanction object as a json serialized request body
     * @returns {Promise<void>}
     * @memberof SanctionController
     */
    @Response<Errors.BadRequestError>(400)
    @Response<Errors.NotFoundError>(404)
    @Response<Sanction>(200)
    @Path(":id")
    @PUT
    async update(@PathParam('id') id: string, body: Sanction): Promise<void> {
        const sanctionUpdate: Sanction = body;
        if (!sanctionUpdate) {
            return Promise.reject(new Errors.BadRequestError);
        }
        console.log(sanctionUpdate);
        try {
            const updatedSanction = await this.sanctionService.updateSanction(id, sanctionUpdate);
            const response = new Resource.Builder(Resource.Self.Sanction, updatedSanction.id).build();
            return response.toJSON();
        } catch (error) {
            return Promise.reject(error);
        }
    }


}

