import { RegistrableController } from '../../RegisterableController';
import { inject, injectable } from "inversify";
import TYPES from '../../../provider/Types';
import { RemittanceService } from '../../../service/RemittanceService';
import { Builder } from "../../../provider/validator";
import * as Resource from "../ResourceBuilder";
import { Remittance, RemittanceStatus } from '../../../model/Remittance'


import { Path, PathParam, POST, GET, PUT, DELETE, Errors, Return, Context, ServiceContext, PATCH, QueryParam } from "typescript-rest";
import { Tags, Produces, Response } from 'typescript-rest-swagger';
import container from '../../../provider/inversify.config';
import { CustomerService } from '../../../service/CustomerService';
import { Customer } from '../../../model/Customer';
import { UserService } from '../../../service/UserService';
import { SortOrder, QueryCondition } from '../../../provider/QueryConditionBuilder';

/**
 * Remittance resource controller class which exposes the Remittance apis
 *
 * @export
 * @class RemittanceController
 * @implements {RegistrableController}
 */
@Path("/v1/remittances")
@Tags('Remittance Controller')
@Produces('application/json')
@injectable()
export class RemittanceController implements RegistrableController {

    @inject(TYPES.RemittanceService)
    private remittanceService: RemittanceService =
        container.get<RemittanceService>(TYPES.RemittanceService);

    @inject(TYPES.CustomerService)
    private customerService: CustomerService =
        container.get<CustomerService>(TYPES.CustomerService);

    @inject(TYPES.UserService)
    private userService: UserService =
        container.get<UserService>(TYPES.UserService);

    /**
     * Creates a new Remittance resource
     *
     * @param {Remittance} body a json serialized Remittance object as a request body.
     * @returns {Promise<any>}
     * @memberof RemittanceController
     */
    @Response<Errors.BadRequestError>(400)
    @Response<Return.NewResource<Remittance>>(201, "Created")
    @POST

    async create(body: Remittance): Promise<any> {
        let remittance: Remittance;
        try {
            remittance = Builder<Remittance>()
                .customers(body.customers)
                .users(body.users)
                .value(body.value)
                .extras(body.extras,false)
                .status(RemittanceStatus.NEW)
                .build()

        } catch (error) {
            console.error(error);
            return Promise.reject(Errors.BadRequestError);
        }

        try {
            const [sender, beneficiary] = await Promise.all([
                this.customerService.createCustomer(remittance.customers.sender as Customer),
                this.customerService.createCustomer(remittance.customers.beneficiary as Customer),
            ]);
            remittance.customers.sender = { id: sender.id, name: sender.name };
            remittance.customers.beneficiary = { id: beneficiary.id, name: beneficiary.name };
            const createdRemittance = await this.remittanceService.createRemittance(remittance);
            return new Return.NewResource<Remittance>(`${Resource.Self.Remittance}/${createdRemittance.id}`);
        } catch (error) {
            console.error(error);
            return Promise.reject(error);
        }
    }
    /**
     * This endpoint deletes a Remittance resource specified by the id in the path.
     * It is not implemented by the server yet and a NotImplementedError will be thrown as a response.
     *
     * @param {string} id firebase authentication Remittance id: uid
     * @returns {Promise<void>}
     * @memberof RemittanceController
     */
    @Response<Errors.NotImplementedError>(501)
    @DELETE
    @Path(":id")
    async delete(@PathParam('id') id: string): Promise<void> {
        return Promise.reject(new Errors.NotImplementedError());
    }

    /**
     * It lists all the available Remittances. Optional query parameters can be included to search for a specific resource.
     * In case there is no resource in the Remittance collection, a NotFoundError might be thrown
     *
     * @returns {Promise<void>}
     * @memberof RemittanceController
     */
    @Response<Errors.NotFoundError>(404)
    @Response<Array<Remittance>>(200)
    @GET
    public async get(@QueryParam("limit") limit?: number,
        @QueryParam("order_by") orderBy?: string,
        @QueryParam("sort_order") sortOrder?: SortOrder,
        @QueryParam("page") page?: number): Promise<void> {
        try {
            const condition = new QueryCondition()
                .withOrderBy(orderBy)
                .withSortOrder(sortOrder)
                .withPage(page)
                .withLimit(limit);
            const remittances = await this.remittanceService.getRemittances(condition);
            if (remittances.length > 0) {
                const response = new Resource.Builder(Resource.Self.Remittance + condition.toQueryString(), null, {
                    Remittances: remittances.map(remittance => {
                        return new Resource.Builder(Resource.Self.Remittance,
                            remittance.id, { id: remittance.id }
                        ).build();
                    })
                }).build();

                return response.toJSON();
            } else {
                throw new Errors.NotFoundError();
            }
        } catch (error) {
            console.log(error);
            return Promise.reject(error);
        }
    }

    /**
     * This endpoint returns a specific Remittance resource specified by the id in the path.
     * If no Remittance is found, a NotFoundError will be thrown.
     *
     * @param {string} id firebase authentication Remittance id: uid
     * @returns {Promise<void>}
     * @memberof RemittanceController
     */
    @Response<Errors.NotFoundError>(404)
    @Response<Remittance>(200)
    @GET
    @Path(":id")
    async getById(@PathParam('id') id: string): Promise<void> {
        try {
            const remittance = await this.remittanceService.getRemittanceById(id);
            if (remittance) {
                const response = new Resource.Builder(Resource.Self.Remittance, remittance.id, remittance)
                    .build();
                return response.toJSON();
            } else {
                throw new Errors.NotFoundError();
            }
        } catch (error) {
            console.log(error);
            return Promise.reject(error);
        }
    }

    /**
     * This endpoint updates a Remittance resource from the modified data in the request body.
     * If the request body is not a typeOf Remittance, a BadRequestError might be thrown. In case
     * a Remittance cannot be found by the specified id in the request path, a NotFoundError will be thrown.
     *
     * @param {string} id firebase authentication Remittance id: uid
     * @param {Remittance} body updated Remittance object as a json serialized request body
     * @returns {Promise<void>}
     * @memberof RemittanceController
     */
    @Response<Errors.BadRequestError>(400)
    @Response<Errors.NotFoundError>(404)
    @Response<Remittance>(200)
    @Path(":id")
    @PUT
    async update(@PathParam('id') id: string, body: Remittance): Promise<void> {
        const remittanceUpdate: Remittance = body;
        if (!remittanceUpdate) {
            return Promise.reject(new Errors.BadRequestError());
        }
        console.log(remittanceUpdate);
        try {
            const updatedRemittance = await this.remittanceService.updateRemittance(id, remittanceUpdate);
            const response = new Resource.Builder(Resource.Self.Remittance, updatedRemittance.id).build();
            return response.toJSON();
        } catch (error) {
            return Promise.reject(error);
        }
    }

    /**
 * This endpoint updates a Remittance resource from the modified data in the request body.
 * In case a Remittance cannot be found by the specified id in the request path, a NotFoundError will be thrown.
 *
 * @param {string} id firebase authentication Remittance id: uid
 * @param {Remittance} body updated Remittance object as a json serialized request body
 * @returns {Promise<void>}
 * @memberof RemittanceController
 */
    @Response<Errors.BadRequestError>(400)
    @Response<Errors.NotFoundError>(404)
    @Response<Remittance>(200)
    @Path(":id")
    @PATCH
    async updateRemittanceOwner(@PathParam('id') id: string, body: any): Promise<void> {
        const remittanceUpdate = body;
        if (!remittanceUpdate) {
            return Promise.reject(new Errors.BadRequestError());
        }
        console.log(remittanceUpdate);
        try {
            const updatedRemittance = await this.remittanceService.updateRemittanceOwner(id, remittanceUpdate);
            const response = new Resource.Builder(Resource.Self.Remittance, updatedRemittance.id).build();
            return response.toJSON();
        } catch (error) {
            return Promise.reject(error);
        }
    }

}

