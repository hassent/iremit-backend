import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { HttpError, Server } from "typescript-rest";

import * as path from 'path';
import {ServiceAccount} from '../provider/ServiceAccount';
import * as express from 'express';
import * as bodyParser from "body-parser";
import * as cors from 'cors';
import TYPES from '../provider/Types';
import container from '../provider/inversify.config';
import { RegistrableController } from "./RegisterableController";

class IRemit {
    public app: express.Application;

    constructor() {
        this.initFirebase();
        this.app = express();
        this.config();
        this.route();
        this.errorHandler();
    }
    public initFirebase(): void {

        admin.initializeApp({
            credential: admin.credential.cert(ServiceAccount),
            databaseURL: "https://iremit-isheger.firebaseio.com"
        });
    }
    public config(): void {
        this.app.use(express.json());
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(cors({ origin: true }));
        //this.app.use(AuthGuard.authenticate);

        this.app.use('/v1/', (req, res, next) => {
            req.headers['accept-version'] = '1.0';
            console.log('Processing request: ', req.originalUrl);
            next();
        });
    }

    public route(): void {

        const controllersV1: RegistrableController[] = container.getAll<RegistrableController>(TYPES.ControllerV1);
        Server.buildServices(this.app);
        controllersV1.forEach(controller => {
            Server.buildServices(express.Router(), controller);
        });
        this.app.use('/v1/docs', express.static(path.join(__dirname, '../swagger')));
        this.app.use('*', (req, res) => {
            res.status(404).send("The requested route is not available");
        });
    }

    public errorHandler(): void {
        /**
         * Intercept errors and produce a json response instead of the default express error handler htlm
         */
        this.app.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
            function isHttpError(arg: any): arg is HttpError {
                return arg.message !== undefined && arg.statusCode !== undefined;
            }

            if (isHttpError(err)) {
                if (res.headersSent) { // important to allow default error handler to close connection if headers already sent
                    next(err);
                }
                res.set("Content-Type", "application/json");
                res.status(err.statusCode);
                res.json({ error: err.message, code: err.statusCode });
            } else {
                res.status(500).json({ error: err.message, code: 500 })
            }
        });
    }
}


//Http express api
export const api = functions.https.onRequest(new IRemit().app);