import {inject, injectable} from "inversify";
import TYPES from "../provider/Types";
import {SanctionRepository} from '../repository/SanctionRepository';
import {Sanction} from "../model/Sanction";
import { QueryCondition } from "../provider/QueryConditionBuilder";

export interface SanctionService {
    getSanctionById(id: string): Promise<Sanction>;

    getSanctions(condition?: QueryCondition): Promise<Array<Sanction>>;

    deleteSanction(id: string): Promise<void>;

    createSanction(sanction: Sanction): Promise<Sanction>;

    updateSanction(id: string, sanction: Sanction): Promise<Sanction>;

}

@injectable()
export class SanctionServiceImpl implements SanctionService {

    private sanctionRepo: SanctionRepository;

    public constructor(@inject(TYPES.SanctionRepository)sanctionRepo: SanctionRepository) {
        this.sanctionRepo = sanctionRepo;
    }

    public async createSanction(sanction: Sanction): Promise<Sanction> {
        const result = await this.sanctionRepo.create(sanction);
        return result.data() as Sanction;
    }

    async deleteSanction(id: string): Promise<void> {
        await this.sanctionRepo.deleteById(id);
    }

    async getSanctionById(id: string): Promise<Sanction> {
        const result = await this.sanctionRepo.findById(id);
        return result.data() as Sanction;
    }

    async getSanctions(condition?: QueryCondition): Promise<Array<Sanction>> {
        const result = await this.sanctionRepo.findAll(condition);
        return result.docs.map(u => u.data() as Sanction)
    }

    async updateSanction(id: string, sanction: Sanction): Promise<Sanction> {
        const result = await this.sanctionRepo.update(id, sanction);
        return result.data() as Sanction;
    }

}