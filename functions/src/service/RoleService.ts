import {inject, injectable} from "inversify";
import TYPES from "../provider/Types";
import {RoleRepository} from '../repository/RoleRepository';
import {Role, Permission} from "../model/Role";
import { QueryCondition } from "../provider/QueryConditionBuilder";

export interface RoleService {
    getRoleById(id: string): Promise<Role>;

    getRoles(condition?: QueryCondition): Promise<Array<Role>>;

    deleteRole(id: string): Promise<void>;

    createRole(role: Role): Promise<Role>;

    updateRole(id: string, permissions: Array<Permission>): Promise<Role>;


}

@injectable()
export class RoleServiceImpl implements RoleService {

    private roleRepo: RoleRepository;

    public constructor(@inject(TYPES.RoleRepository)roleRepo: RoleRepository) {
        this.roleRepo = roleRepo;
    }

    public async createRole(role: Role): Promise<Role> {
        const result = await this.roleRepo.create(role);
        return result.data() as Role;
    }

    async deleteRole(id: string): Promise<void> {
        await this.roleRepo.deleteById(id);
    }

    async getRoleById(id: string): Promise<Role> {
        const result = await this.roleRepo.findById(id);
        return result.data() as Role;
    }

    async getRoles(condition?: QueryCondition): Promise<Array<Role>> {
        const result = await this.roleRepo.findAll(condition);
        return result.docs.map(u => u.data() as Role)
    }

    async updateRole(id: string, permissions: Array<Permission>): Promise<Role> {
        const result = await this.roleRepo.update(id, permissions);
        return result.data() as Role;
    }

}