import {inject, injectable} from "inversify";
import TYPES from "../provider/Types";
import {Event} from "../model/Event";
import {EventRepository} from "../repository/EventRepository";
import { QueryCondition } from "../provider/QueryConditionBuilder";

export interface EventService {
    getEventById(id: string): Promise<Event>;

    getEvents(condition?: QueryCondition): Promise<Array<Event>>;

    deleteEvent(id: string): Promise<void>;

    createEvent(event: Event): Promise<Event>;

    updateEvent(id: string, event: Event): Promise<Event>;
}

@injectable()
export class EventServiceImpl implements EventService {

    @inject(TYPES.EventRepository)
    private eventRepo: EventRepository;

    async createEvent(event: Event): Promise<Event> {
        const result = await this.eventRepo.create(event);
        return result.data() as Event;
    }

    async deleteEvent(id: string): Promise<void> {
        await this.eventRepo.deleteById(id);
    }

    async getEventById(id: string): Promise<Event> {
        const result = await this.eventRepo.findById(id);
        return result.data() as Event;
    }

    async getEvents(condition?: QueryCondition): Promise<Array<Event>> {
        const result = await this.eventRepo.findAll(condition);
        return result.docs.map(e => e.data() as Event);
    }

    async updateEvent(id: string, event: Event): Promise<Event> {
        const result = await this.eventRepo.update(id, event);
        return result.data() as Event;
    }
}