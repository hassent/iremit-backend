import {inject, injectable} from "inversify";
import TYPES from "../provider/Types";
import {DefaultRepository} from '../repository/defaultRepository';
import {Default} from "../model/default";
import { QueryCondition } from "../provider/QueryConditionBuilder";

export interface DefaultService {
    getDefaultById(id: string): Promise<Default>;

    getDefaults(condition?: QueryCondition): Promise<Array<Default>>;

    deleteDefault(id: string): Promise<void>;

    createDefault(Default: Default): Promise<Default>;

    updateDefault(id: string, Default: Default): Promise<Default>;
}

@injectable()
export class DefaultServiceImpl implements DefaultService {

    @inject(TYPES.DefaultRepository)
    private DefaultRepo: DefaultRepository;

    async createDefault(e: Default): Promise<Default> {
        const result = await this.DefaultRepo.create(e);
        return result.data() as Default;
    }

    async deleteDefault(id: string): Promise<void> {
        await this.DefaultRepo.deleteById(id);
    }

    async getDefaultById(id: string): Promise<Default> {
        const result = await this.DefaultRepo.findById(id);
        return result.data() as Default;
    }

    async getDefaults(condition?: QueryCondition): Promise<Array<Default>> {
        const result = await this.DefaultRepo.findAll(condition);
        return result.docs.map(a => a.data() as Default);
    }

    async updateDefault(id: string, adComp: Default): Promise<Default> {
        const result = await this.DefaultRepo.update(id, adComp);
        return result.data() as Default;
    }
}