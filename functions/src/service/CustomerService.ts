import { inject, injectable } from "inversify";
import TYPES from "../provider/Types";
import { CustomerRepository } from '../repository/CustomerRepository';
import { Customer } from "../model/Customer";
import { DocumentSnapshot } from "firebase-functions/lib/providers/firestore";
import { QueryCondition } from "../provider/QueryConditionBuilder";

export interface CustomerService {
    getCustomerById(id: string): Promise<Customer>;

    getCustomers(condition?: QueryCondition): Promise<Array<Customer>>;

    deleteCustomer(id: string): Promise<void>;

    createCustomer(customer: Customer): Promise<Customer>;

    updateCustomer(id: string, customer: Customer): Promise<Customer>;

}

@injectable()
export class CustomerServiceImpl implements CustomerService {

    @inject(TYPES.CustomerRepository)
    private customerRepo: CustomerRepository;

    public constructor(@inject(TYPES.CustomerRepository) customerRepo: CustomerRepository) {
        this.customerRepo = customerRepo;
    }

    async createCustomer(customer: Customer): Promise<Customer> {
        const filter = [
            { lhs: "msisdn", whereOp: "==", rhs: customer.msisdn },
            { lhs: "name", whereOp: "==", rhs: customer.name },
            { lhs: "type", whereOp: "==", rhs: customer.type },
        ];
        const condition = new QueryCondition();
        condition.filters = filter;
        const customers = await this.customerRepo.findAll(condition);
        let result: DocumentSnapshot;
        if (!customers.empty) {
            result = await this.customerRepo.incrementCounter(customers.docs[0].id);
        } else {
            customer.count = 1;
            result = await this.customerRepo.create(customer);
        }

        return result.data() as Customer;
    }

    async deleteCustomer(id: string): Promise<void> {
        await this.customerRepo.deleteById(id);
    }

    async getCustomerById(id: string): Promise<Customer> {
        const result = await this.customerRepo.findById(id);
        return result.data() as Customer;
    }

    async getCustomers(condition?: QueryCondition): Promise<Array<Customer>> {
        const result = await this.customerRepo.findAll(condition);
        return result.docs.map(a => a.data() as Customer);
    }

    async updateCustomer(id: string, customer: Customer): Promise<Customer> {
        const result = await this.customerRepo.update(id, customer);
        return result.data() as Customer;
    }
}