import { inject, injectable } from "inversify";
import TYPES from "../provider/Types";
import { RemittanceRepository } from '../repository/RemittanceRepository';
import { Remittance, RemittanceOwner } from "../model/Remittance";
import { QueryCondition } from "../provider/QueryConditionBuilder";

export interface RemittanceService {
    getRemittanceById(id: string): Promise<Remittance>;

    getRemittances(condition?: QueryCondition): Promise<Array<Remittance>>;

    deleteRemittance(id: string): Promise<void>;

    createRemittance(remittance: Remittance): Promise<Remittance>;

    updateRemittance(id: string, remittance: Remittance): Promise<Remittance>;

    updateRemittanceOwner(id: string, body: any): Promise<Remittance>;
}

@injectable()
export class RemittanceServiceImpl implements RemittanceService {

    private remittanceRepo: RemittanceRepository;

    public constructor(@inject(TYPES.RemittanceRepository) remittanceRepo: RemittanceRepository) {
        this.remittanceRepo = remittanceRepo;
    }

    async createRemittance(remittance: Remittance): Promise<Remittance> {
        const result = await this.remittanceRepo.create(remittance);
        return result.data() as Remittance;
    }

    async deleteRemittance(id: string): Promise<void> {
        await this.remittanceRepo.deleteById(id);
    }

    async getRemittanceById(id: string): Promise<Remittance> {
        const result = await this.remittanceRepo.findById(id);
        return result.data() as Remittance;
    }

    async getRemittances(condition?: QueryCondition): Promise<Array<Remittance>> {
        const result = await this.remittanceRepo.findAll(condition);
        return result.docs.map(a => a.data() as Remittance);
    }

    async updateRemittance(id: string, remittance: Remittance): Promise<Remittance> {
        const result = await this.remittanceRepo.update(id, remittance);
        return result.data() as Remittance;
    }

    async updateRemittanceOwner(id: string, body: any): Promise<Remittance> {
        const result = await this.remittanceRepo.updateRemittanceOwner(id, body);
        return result.data() as Remittance;
    }
}