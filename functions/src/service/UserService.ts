import {inject, injectable} from "inversify";
import TYPES from "../provider/Types";
import {UserRepository} from '../repository/UserRepository';
import {User, UserStatus} from "../model/User";
import { QueryCondition } from "../provider/QueryConditionBuilder";

export interface UserService {
    getUserById(id: string): Promise<User>;

    getUsers(condition?: QueryCondition): Promise<Array<User>>;

    deleteUser(id: string): Promise<void>;

    createUser(user: User): Promise<User>;

    updateUser(id: string, user: User): Promise<User>;

    updateUserStatus(id: string, status: UserStatus): Promise<User>;

}

@injectable()
export class UserServiceImpl implements UserService {

    private userRepo: UserRepository;

    public constructor(@inject(TYPES.UserRepository)userRepo: UserRepository) {
        this.userRepo = userRepo;
    }

    public async createUser(user: User): Promise<User> {
        const result = await this.userRepo.create(user);
        return result.data() as User;
    }

    async deleteUser(id: string): Promise<void> {
        await this.userRepo.deleteById(id);
    }

    async getUserById(id: string): Promise<User> {
        const result = await this.userRepo.findById(id);
        return result.data() as User;
    }

    async getUsers(condition?: QueryCondition): Promise<Array<User>> {
        const result = await this.userRepo.findAll(condition);
        return result.docs.map(u => u.data() as User)
    }

    async updateUser(id: string, user: User): Promise<User> {
        const result = await this.userRepo.update(id, user);
        return result.data() as User;
    }

    async updateUserStatus(id: string, status: UserStatus): Promise<User> {
        const result = await this.userRepo.updateStatus(id, status);
        return result.data() as User;
    }
}