import { inject, injectable } from "inversify";
import TYPES from "../provider/Types";
import { DepositRepository } from '../repository/DepositRepository';
import { Deposit } from "../model/Deposit";
import { QueryCondition } from "../provider/QueryConditionBuilder";

export interface DepositService {
    getDepositById(id: string): Promise<Deposit>;

    getDeposits(condition?: QueryCondition): Promise<Array<Deposit>>;

    deleteDeposit(id: string): Promise<void>;

    createDeposit(deposit: Deposit): Promise<Deposit>;

    updateDeposit(id: string, deposit: Deposit): Promise<Deposit>;

}

@injectable()
export class DepositServiceImpl implements DepositService {

    private depositRepo: DepositRepository;

    public constructor(@inject(TYPES.DepositRepository) depositRepo: DepositRepository) {
        this.depositRepo = depositRepo;
    }

    async createDeposit(deposit: Deposit): Promise<Deposit> {
        const result = await this.depositRepo.create(deposit);
        return result.data() as Deposit;
    }

    async deleteDeposit(id: string): Promise<void> {
        await this.depositRepo.deleteById(id);
    }

    async getDepositById(id: string): Promise<Deposit> {
        const result = await this.depositRepo.findById(id);
        return result.data() as Deposit;
    }

    async getDeposits(condition?: QueryCondition): Promise<Array<Deposit>> {
        const result = await this.depositRepo.findAll(condition);
        return result.docs.map(a => a.data() as Deposit);
    }

    async updateDeposit(id: string, deposit: Deposit): Promise<Deposit> {
        const result = await this.depositRepo.update(id, deposit);
        return result.data() as Deposit;
    }

}