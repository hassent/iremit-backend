import {injectable} from "inversify";
import {CrudRepository} from "./CrudRepository";
import {User, UserStatus} from "../model/User";
import {UniqueConstraintError} from "../error/UniqueConstraintError";
import {DB} from "./DB";
import DocumentSnapshot = FirebaseFirestore.DocumentSnapshot;
import QuerySnapshot = FirebaseFirestore.QuerySnapshot;
import Query = FirebaseFirestore.Query;
import { QueryCondition } from "../provider/QueryConditionBuilder";

export interface UserRepository extends CrudRepository<User> {
    updateStatus(id: string, status: UserStatus): Promise<DocumentSnapshot>;
}

@injectable()
export class UserRepositoryImpl extends DB implements UserRepository {


    public async create(user: User): Promise<DocumentSnapshot> {

        const uniqueUser = await super.userCol().where('mail', '==', user.email).get();
        if (!uniqueUser.empty) {
            return Promise.reject(new UniqueConstraintError("mail is not unique"));
        }

        const docRef = super.userCol().doc(user.id);
        user.id = docRef.id;
        user.created = new Date();
        await docRef.set(user);
        return await docRef.get();
    }

    async deleteById(id: string): Promise<void> {
        return undefined;
    }

    async findAll(condition?: QueryCondition): Promise<QuerySnapshot> {
        let colRef = super.userCol();
        return super.query(colRef, condition).then(q => q.get());
    }

    async findById(id: string): Promise<DocumentSnapshot> {
        return await super.userCol().doc(id).get();
    }

    async update(id: string, updatedUser: User): Promise<DocumentSnapshot> {
        const docRef = super.userCol().doc(id);
        await docRef.update(updatedUser);
        return await docRef.get();
    }

    async updateStatus(id: string, status: UserStatus): Promise<DocumentSnapshot> {
        const docRef = super.userCol().doc(id);
        await docRef.update({"status":status});
        return await docRef.get();
    }


}