import { injectable } from "inversify";
import { CrudRepository } from "./CrudRepository";

import { UniqueConstraintError } from "../error/UniqueConstraintError";
import { DB } from "./DB";
import DocumentSnapshot = FirebaseFirestore.DocumentSnapshot;
import QuerySnapshot = FirebaseFirestore.QuerySnapshot;
import Query = FirebaseFirestore.Query;
import { QueryCondition } from "../provider/QueryConditionBuilder";
import { Role, Permission } from "../model/Role";

export interface RoleRepository extends CrudRepository<Role> { }

@injectable()
export class RoleRepositoryImpl extends DB implements RoleRepository {


    public async create(role: Role): Promise<DocumentSnapshot> {

        const uniqueUser = await super.rolCol().doc(role.id).get();
        if (!uniqueUser) {
            return Promise.reject(new UniqueConstraintError("mail is not unique"));
        }

        const docRef = super.rolCol().doc(role.id);
        await docRef.set(role);
        return await docRef.get();
    }

    async deleteById(id: string): Promise<void> {
        return undefined;
    }

    async findAll(condition?: QueryCondition): Promise<QuerySnapshot> {
        let colRef = super.rolCol();
        return super.query(colRef, condition).then(q => q.get());
    }

    async findById(id: string): Promise<DocumentSnapshot> {
        return await super.rolCol().doc(id).get();
    }

    async update(id: string, permissions: Array<Permission>): Promise<DocumentSnapshot> {
        const docRef = super.rolCol().doc(id);
        await docRef.update({ "permissions": permissions });
        return await docRef.get();
    }


}