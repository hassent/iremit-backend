import {Entity} from "../model/Entity";
import { QueryCondition } from "../provider/QueryConditionBuilder";
import DocumentSnapshot = FirebaseFirestore.DocumentSnapshot;
import QuerySnapshot = FirebaseFirestore.QuerySnapshot;

export interface CrudRepository<T extends Entity> {
    deleteById(id: string): Promise<void>;

    findById(id: string): Promise<DocumentSnapshot>;

    findAll(condition?: QueryCondition): Promise<QuerySnapshot>;

    create(e: T): Promise<DocumentSnapshot>;

    update(id: string, e: any): Promise<DocumentSnapshot>;
}