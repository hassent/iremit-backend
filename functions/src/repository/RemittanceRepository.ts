import { injectable } from "inversify";
import { CrudRepository } from "./CrudRepository";
import { Remittance, RemittanceOwner } from "../model/Remittance";
import { DB } from "./DB";
import DocumentSnapshot = FirebaseFirestore.DocumentSnapshot;
import QuerySnapshot = FirebaseFirestore.QuerySnapshot;
import Query = FirebaseFirestore.Query;
import { QueryCondition } from "../provider/QueryConditionBuilder";
const dot = require('dot-object');

export interface RemittanceRepository extends CrudRepository<Remittance> {
    updateRemittanceOwner(id: string, body: any): Promise<DocumentSnapshot>;
}

@injectable()
export class RemittanceRepositoryImpl extends DB implements RemittanceRepository {
    async create(remittance: Remittance): Promise<DocumentSnapshot> {
        const docRef = super.remittanceCol().doc();
        remittance.id = docRef.id;

        await docRef.set(remittance);
        return await docRef.get();
    }

    async deleteById(id: string): Promise<void> {
        return undefined;
    }

    async findAll(condition?: QueryCondition): Promise<QuerySnapshot> {
        let colRef = super.remittanceCol();
        return super.query(colRef, condition).then(q => q.get());
    }

    async findById(id: string): Promise<DocumentSnapshot> {
        return await super.remittanceCol().doc(id).get();
    }

    async update(id: string, e: any): Promise<DocumentSnapshot> {
        const docRef = super.remittanceCol().doc(id);
        const current = await docRef.get().then((c) => {
            return c.data();
        });
        for (const key in e) {
            if (key != null && typeof key === 'object') {
                for (const nestedKey in key) {
                    current[key][nestedKey] = e[key][nestedKey]
                }
            } else
                current[key] = e[key];
        }
        await docRef.update(current);
        return await docRef.get();
    }

    async updateRemittanceOwner(id: string, body: any): Promise<DocumentSnapshot> {
        const docRef = super.remittanceCol().doc(id);
        await docRef.update(dot.dot(body));
        return await docRef.get();
    }




}