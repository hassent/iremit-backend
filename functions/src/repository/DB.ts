import * as admin from 'firebase-admin'
import { injectable } from "inversify";
import 'reflect-metadata';
import CollectionReference = FirebaseFirestore.CollectionReference;
import WriteBatch = FirebaseFirestore.WriteBatch;
import Query = FirebaseFirestore.Query;
import { QueryCondition } from "../provider/QueryConditionBuilder";

@injectable()
export class DB {

    private events: string = "events";

    protected users: string = "users";
    protected customers: string = "customers";
    protected defaults: string = "defaults";
    protected remittances: string = "remittances";
    protected roles: string = "roles";
    protected deposits: string = "deposits";
    protected sanctions: string = "UN-Sanction-List";

    protected getDB(): FirebaseFirestore.Firestore {
        return admin.firestore();
    }

    protected batch(): WriteBatch {
        return this.getDB().batch();
    }

    protected eventCol(): CollectionReference {
        return this.getDB().collection(this.events);
    }

    protected userCol(): CollectionReference {
        return this.getDB().collection(this.users);
    }

    protected remittanceCol(): CollectionReference {
        return this.getDB().collection(this.remittances);
    }

    protected defaultCol(): CollectionReference {
        return this.getDB().collection(this.defaults);
    }

    protected customerCol(): CollectionReference {
        return this.getDB().collection(this.customers);
    }

    protected rolCol(): CollectionReference {
        return this.getDB().collection(this.roles);
    }

    protected depCol(): CollectionReference {
        return this.getDB().collection(this.deposits);
    }

    protected sanctionCol(): CollectionReference {
        return this.getDB().collection(this.sanctions);
    }

    protected async query(colRef: CollectionReference, condition?: QueryCondition): Promise<Query> {
        let q: Query = colRef;
        const isPaged = condition.orderBy && condition.limit && condition.page && condition.page > 1;
        if (condition) {
            for (const filter of condition.filters) {
                q = q.where(filter.lhs, <any>filter.whereOp, filter.rhs)    //Have to cast to any for compiler..
            }
            if (condition.orderBy) {
                if (condition.sortOrder)
                    q = q.orderBy(condition.orderBy, condition.sortOrder);
                else
                    q = q.orderBy(condition.orderBy);
            }
            if (condition.limit) {
                if (isPaged)
                    condition.limit *= (condition.page - 1);
                q = q.limit(condition.limit);
            }
            if (isPaged) {
                const lastSeen = await q.get().then(docSnapshots => {
                    return docSnapshots.docs[docSnapshots.docs.length - 1];
                });
                q = q.startAfter(lastSeen);
            }
        }
        return Promise.resolve(q);
    }

}