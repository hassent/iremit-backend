import {injectable} from "inversify";
import {CrudRepository} from "./CrudRepository";
import {DB} from "./DB";
import {Event} from "../model/Event";
import DocumentSnapshot = FirebaseFirestore.DocumentSnapshot;
import QuerySnapshot = FirebaseFirestore.QuerySnapshot;
import Query = FirebaseFirestore.Query;
import { QueryCondition } from "../provider/QueryConditionBuilder";

export interface EventRepository extends CrudRepository<Event> {

}

@injectable()
export class EventRepositoryImpl extends DB implements EventRepository {
    async create(e: Event): Promise<DocumentSnapshot> {
        return undefined;
    }

    async deleteById(id: string): Promise<void> {
        return undefined;
    }

    async findAll(condition?: QueryCondition): Promise<QuerySnapshot> {
        let colRef = super.eventCol();
        return super.query(colRef, condition).then(q => q.get());
    }

    async findById(id: string): Promise<DocumentSnapshot> {
        return undefined;
    }

    async update(id: string, e: any): Promise<DocumentSnapshot> {
        return undefined;
    }

}