import { injectable } from "inversify";
import { CrudRepository } from "./CrudRepository";
import { Customer } from "../model/Customer";
import { DB } from "./DB";
import { QueryCondition } from "../provider/QueryConditionBuilder";
import DocumentSnapshot = FirebaseFirestore.DocumentSnapshot;
import QuerySnapshot = FirebaseFirestore.QuerySnapshot;
import Query = FirebaseFirestore.Query;

export interface CustomerRepository extends CrudRepository<Customer> {
    incrementCounter(id: string): Promise<DocumentSnapshot>;
}

@injectable()
export class CustomerRepositoryImpl extends DB implements CustomerRepository {
    async create(customer: Customer): Promise<DocumentSnapshot> {
        const docRef = super.customerCol().doc();
        customer.id = docRef.id;

        await docRef.set(customer);
        return await docRef.get();
    }

    async deleteById(id: string): Promise<void> {
        return undefined;
    }

    async findAll(condition?: QueryCondition): Promise<QuerySnapshot> {
        let colRef = super.customerCol();
        return super.query(colRef, condition).then(q => q.get());
    }

    async findById(id: string): Promise<DocumentSnapshot> {
        return await super.customerCol().doc(id).get();
    }

    async update(id: string, e: any): Promise<DocumentSnapshot> {
        const docRef = super.customerCol().doc(id);
        const current = await docRef.get().then((c) => {
            return c.data();
        });
        for (const key in e) {
            if (key != null && typeof key === 'object') {
                for (const nestedKey in key) {
                    current[key][nestedKey] = e[key][nestedKey]
                }
            } else
                current[key] = e[key];
        }
        await docRef.update(current);
        return await docRef.get();
    }

    async incrementCounter(id: string): Promise<DocumentSnapshot> {
        const docRef = super.customerCol().doc(id);
        const count = await super.customerCol().doc(id).get().then(c => { return c.data().count });
        await docRef.update({count: count+1});
        return await docRef.get();
    }




}