import {injectable} from "inversify";
import {CrudRepository} from "./CrudRepository";
import {Sanction} from "../model/Sanction";
import {UniqueConstraintError} from "../error/UniqueConstraintError";
import {DB} from "./DB";
import DocumentSnapshot = FirebaseFirestore.DocumentSnapshot;
import QuerySnapshot = FirebaseFirestore.QuerySnapshot;
import Query = FirebaseFirestore.Query;
import { QueryCondition } from "../provider/QueryConditionBuilder";

export interface SanctionRepository extends CrudRepository<Sanction> {
 }

@injectable()
export class SanctionRepositoryImpl extends DB implements SanctionRepository {


    public async create(sanction: Sanction): Promise<DocumentSnapshot> {

        const uniqueSanctionList = await super.sanctionCol()
        .where('firstName', '==', sanction.firstName)
        .where('middleName', '==', sanction.middleName)
        .where('lastName', '==', sanction.lastName)
        .where('familyName', '==', sanction.familyName)
        .get();
        if (!uniqueSanctionList.empty) {
            return Promise.reject(new UniqueConstraintError("mail is not unique"));
        }

        const docRef = super.sanctionCol().doc(sanction.id);
        sanction.id = docRef.id;
        await docRef.set(sanction);
        return await docRef.get();
    }

    async deleteById(id: string): Promise<void> {
        return undefined;
    }

    async findAll(condition?: QueryCondition): Promise<QuerySnapshot> {
        let colRef = super.sanctionCol();
        return super.query(colRef, condition).then(q => q.get());
    }

    async findById(id: string): Promise<DocumentSnapshot> {
        return await super.sanctionCol().doc(id).get();
    }

    async update(id: string, updatedSanction: Sanction): Promise<DocumentSnapshot> {
        const docRef = super.sanctionCol().doc(id);
        await docRef.update(updatedSanction);
        return await docRef.get();
    }

}