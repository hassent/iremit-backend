import {injectable} from "inversify";
import {CrudRepository} from "./CrudRepository";
import {Deposit, DepositStatus} from "../model/Deposit";
import {UniqueConstraintError} from "../error/UniqueConstraintError";
import {DB} from "./DB";
import DocumentSnapshot = FirebaseFirestore.DocumentSnapshot;
import QuerySnapshot = FirebaseFirestore.QuerySnapshot;
import Query = FirebaseFirestore.Query;
import { QueryCondition } from "../provider/QueryConditionBuilder";

export interface DepositRepository extends CrudRepository<Deposit> {
    updateStatus(id: string, status: DepositStatus): Promise<DocumentSnapshot>;
}

@injectable()
export class DepositRepositoryImpl extends DB implements DepositRepository {


    public async create(deposit: Deposit): Promise<DocumentSnapshot> {

       const docRef = super.depCol().doc(deposit.id);
        deposit.id = docRef.id;
        deposit.timestamps.created = new Date();
        await docRef.set(deposit);
        return await docRef.get();
    }

    async deleteById(id: string): Promise<void> {
        return undefined;
    }

    async findAll(condition?: QueryCondition): Promise<QuerySnapshot> {
        let colRef = super.depCol();
        return super.query(colRef, condition).then(q => q.get());
    }

    async findById(id: string): Promise<DocumentSnapshot> {
        return await super.depCol().doc(id).get();
    }

    async update(id: string, updatedDeposit: Deposit): Promise<DocumentSnapshot> {
        const docRef = super.depCol().doc(id);
        await docRef.update(updatedDeposit);
        return await docRef.get();
    }

    async updateStatus(id: string, status: DepositStatus): Promise<DocumentSnapshot> {
        const docRef = super.depCol().doc(id);
        await docRef.update({"status":status});
        return await docRef.get();
    }


}