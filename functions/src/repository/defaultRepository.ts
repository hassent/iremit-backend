import { injectable } from "inversify";
import { CrudRepository } from "./CrudRepository";
import { Default } from "../model/default";
import { DB } from "./DB";
import DocumentSnapshot = FirebaseFirestore.DocumentSnapshot;
import QuerySnapshot = FirebaseFirestore.QuerySnapshot;
import Query = FirebaseFirestore.Query;
import { QueryCondition } from "../provider/QueryConditionBuilder";

export interface DefaultRepository extends CrudRepository<Default> { }

@injectable()
export class DefaultRepositoryImpl extends DB implements DefaultRepository {
    async create(e: Default): Promise<DocumentSnapshot> {
        const docRef = super.defaultCol().doc();
        e.id = docRef.id;

        await docRef.set(e);
        return await docRef.get();
    }

    async deleteById(id: string): Promise<void> {
        return undefined;
    }

    async findAll(condition?: QueryCondition): Promise<QuerySnapshot> {
        let colRef = super.defaultCol();
        return super.query(colRef, condition).then(q => q.get());
    }

    async findById(id: string): Promise<DocumentSnapshot> {
        return await super.defaultCol().doc(id).get();
    }

    async update(id: string, e: any): Promise<DocumentSnapshot> {
        const docRef = super.defaultCol().doc(id);
        const current = await docRef.get().then((c) => {
            return c.data();
        });
        for (const key in e) {
            if (key != null && typeof key === 'object') {
                for (const nestedKey in key) {
                    current[key][nestedKey] = e[key][nestedKey]
                }
            } else
                current[key] = e[key];
        }
        await docRef.update(current);
        return await docRef.get();
    }




}