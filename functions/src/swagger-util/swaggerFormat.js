const fs = require('fs');
const jsonFile = "./lib/src/swagger/swagger.json";

let swagger;
/**
 * The generated swagger.json file has missing properties (I don't know why).
 * The swagger-ui complains document error. But it will be fixed by just adding
 * the missing properties in the json file. (the yaml file is as it is)
 *
 * @returns Promise
 */
function addMissing() {
    swagger.definitions["NewResource"] = {
        "description": "Inform that a new resource was created. Server will add a Location header and set status to 201",
        "properties":
            {
                "location":
                    {
                        "type": "string",
                        "description": "To be added to the Location header on response"
                    },
                "body":
                    {
                        "type": "object",
                        "description": "To be added to the response body"
                    }
            },
        "required": ["location"]
    };
    swagger.schemes = ["https"];
    return Promise.resolve(swagger);
}

/**
 * We have investigated the generated swagger.json file contains
 * duplicates in the following object references. We just manullay remove the
 * duplicate to solve the issue. (can be automatic, but it's waste of time)
 *
 * @returns Promise
 */
function removeDuplicate() {
    const defs = swagger.definitions;
    for(let key in defs) {
        if(!defs[key].required) continue;
        let s = new Set(defs[key].required);
        let it = s.values();
        defs[key].required = Array.from(it);
    }

    return Promise.resolve(swagger);
}

/**
 * replace the file
 *
 */
function finalize() {
    fs.writeFile(jsonFile, JSON.stringify(swagger), function (err) {
        if (err) {
            console.log(err);
        }
    });
}

fs.readFile(jsonFile, function read(err, data) {
    if (err) {
        console.error(err.message);
    }
    swagger = JSON.parse(data);
    addMissing().then(
        removeDuplicate().then(
            finalize()
        )
    );

});
