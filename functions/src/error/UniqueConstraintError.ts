import { ConflictError } from "typescript-rest/dist/server-errors";

export class UniqueConstraintError extends ConflictError {
    constructor(m: string) {
        super(m);
        Object.setPrototypeOf(this, UniqueConstraintError.prototype);
    }
}