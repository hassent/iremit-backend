import { Entity } from "./Entity";
export interface Sanction extends Entity {
    firstName: string;
    middleName: string;
    lastName: string;
    familyName?: string;
    country: string;
}