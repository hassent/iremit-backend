import {Entity} from "./Entity";

export interface Event extends Entity {
    name: string;
    type: Type;
    userId?: string,
    email?: string,
    triggeredBy: string,
    timestamp: Date;
    data: any;
}

export enum Type {
    Create = 'CREATE',
    Update = 'UPDATE',
    Delete = 'DELETE'
}

export const MetaData = {
    UserEvent : {eventName:'UserEvent', dataName:'user'},
    RoleEvent : {eventName: 'RoleEvent', dataName:'role'},
    RemittanceEvent : {eventName: 'RemittanceEvent', dataName:'remittance'},
    DefaultEvent : {eventName: 'DefaultEvent', dataName:'default'},
    CustomerEvent : {eventName: 'CustomerEvent', dataName:'customer'},
    DepositEvent : {eventName: 'DepositEvent', dataName:'deposit'}
};
