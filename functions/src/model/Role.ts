import { Entity } from "./Entity";
export enum Permission {
    CAN_SEND_REMITTANCE = "CAN_SEND_REMITTANCE",
    CAN_PAYOUT = "CAN_PAYOUT",
    CAN_APPROVE = "CAN_APPROVE",
    CAN_PRINT_RECEIPT = "CAN_PRINT_RECEIPT",
    CAN_CREATE_SUB_USER = "CAN_CREATE_SUB_USER",
    CAN_GENERATE_REPORT = "CAN_GENERATE_REPORT",
    CAN_DEPOSIT = "CAN_DEPOSIT",
    CAN_APPROVE_DEPOSIT = "CAN_APPROVE_DEPOSIT"
}

export interface Role extends Entity {
    email: string;
    assignee: string;
    permissions: Array<Permission>;
}