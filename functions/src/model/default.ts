import { Entity } from "./Entity";

export interface Default extends Entity {
    [key: string]: {}
}