import { Entity } from './Entity'
export interface Deposit extends Entity {
    status: DepositStatus,
    timestamps: DepositDetail,
    users: DepositDetail,
    value: DepositDetail
}


export interface DepositDetail {
    [key: string]: {} | {
        [key: string]: {}
    }
}

export enum DepositStatus {
    NEW = "NEW",
    PENDING = "PENDING",
    WAITING_APPROVAL = "WAITING_APPROVAL",
    APPROVED = "APPROVED",
    DISAPPROVED = "DISAPPROVED",
    DISCARDED = "DISCARDED",
 }
