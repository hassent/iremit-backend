import { Entity } from './Entity'
import { Permission } from './Role';
export interface Remittance extends Entity {
    customers: RemittanceDetail,
    users: RemittanceDetail,
    value: RemittanceDetail,
    status: RemittanceStatus,
    extras?: RemittanceDetail
}

export interface RemittanceOwner extends Entity {
    status: RemittanceStatus,
    referredBy: string,
    referredTo?: string,
    permission: { [key: string]: boolean }
}

export interface RemittanceDetail {
    [key: string]: {} | {
        [key: string]: {}
    }
}

export enum RemittanceStatus {
    NEW = "NEW",
    PENDING = "PENDING",
    WAITING_APPROVAL = "WAITING_APPROVAL",
    APPROVED = "APPROVED",
    DISAPPROVED = "DISAPPROVED",
    DISCARDED = "DISCARDED",
    PAID = "PAID"
}
export enum PaymentMethod {
    SWISH = "SWISH",
    CASH = "CASH",
    CREDIT = "CREDIT",
    BANK_TRANSFER = "BANK_TRANSFER"
}

const remittance: Remittance = {
    status: RemittanceStatus.NEW,
    customers: {
        "sender": { "name": "abebe" },
        "receiver": { "name": "tola" }
    },
    users: {
        "lg3VDn9LXQRhGUBnm8KoeeLjtsh2": {
            "referredBy": "lg3VDn9LXQRhGUBnm8KoeeLjtsh2",
            "referredTo": "tpcU6JCsTgZBoGmY5p8SJQdT41C2",
            "name": "suleyman",
            "remittanceStatus": RemittanceStatus.APPROVED,
            "permission": {
                "can_edit": true,
                "can_discard": true,
                "can_view": true
            }
        },
        "tpcU6JCsTgZBoGmY5p8SJQdT41C2": {
            "referredBy": "lg3VDn9LXQRhGUBnm8KoeeLjtsh2",
            "referredTo": "recU6JCsTgZBoGmY5p8SJQwbX013",
            "name": "mohammed",
            "remittanceStatus": RemittanceStatus.PENDING,
            "permission": {
                "can_disapprove": true,
                "can_approve": false,
                "can_view": true
            }
        },
        "recU6JCsTgZBoGmY5p8SJQwbX013": {
            "referredBy": "tpcU6JCsTgZBoGmY5p8SJQdT41C2",
            "referredTo": "sEmU6JCsTgZBoGmY5p8SJQwbX013",
            "name": "hassen",
            "remittanceStatus": RemittanceStatus.DISAPPROVED,
            "permission": {
                "can_payout": false,
                "can_view": true
            }
        },
        "sEmU6JCsTgZBoGmY5p8SJQwbX013": {
            "referredBy": "recU6JCsTgZBoGmY5p8SJQwbX013",
            "referredTo": "sEmU6JCsTgZBoGmY5p8SJQwbX013",
            "name": "ali",
            "remittanceStatus": RemittanceStatus.DISAPPROVED,
            "permission": {
                "can_payout": false,
                "can_view": true
            }
        }

    },
    value: {
        "rate": "4.2",
        "from": {
            "amount": "5000",
            "currency": "Swedish Kronenr",
            "symbol": "SEK"
        },
        "to": {
            "amount": "21000",
            "currency": "Ethiopian Birr",
            "symbol": "ETB"
        }
    },
    extras: {
        "payment": {
            "method": PaymentMethod.BANK_TRANSFER,
            "reference": ""
        },
        "remittance_reason": ""
    }
}
