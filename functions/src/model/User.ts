import {Entity} from "./Entity";
/**
 * A user object
 *
 * @export
 * @interface User
 * @extends {Entity}
 */
export interface User extends Entity {
    referrerId:string;
    email: string;
    displayName: string;
    firebaseToken: string;
    created: Date;
    status: UserStatus;
}

export enum UserStatus {
    ACTIVE = "ACTIVE",
    BLOCKED = "BLOCKED",
    PENDING = "PENDING"
}