import { Entity } from './Entity'
export interface Customer extends Entity {
    name: string,
    msisdn: string,
    customerDetail?: CustomerDetail,
    type: CustomerType,
    ownerId: string,
    count?: number
}

export enum CustomerType {
    SENDER = "SENDER",
    BENEFICIARY = "BENEFICIARY"
}

export interface CustomerDetail {
    [key: string]: string | {
        [key: string]: string
    }
}
