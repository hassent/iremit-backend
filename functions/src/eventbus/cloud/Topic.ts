export enum Topic {
    UserTopic = "user-topic",
    RemittanceTopic = "remittance-topic",
    DepositTopic = "deposit-topic",
    CustomerTopic = "customer-topic",
    RoleTopic = "role-topic",
    DefaultTopic = "default-topic"
}