import { Message } from "firebase-functions/lib/providers/pubsub";
import { CloudFunction, Change } from "firebase-functions";
import DocumentSnapshot = FirebaseFirestore.DocumentSnapshot;

export interface SubscriberFunction {
    functionName: string;
    trigger: CloudFunction<Message | DocumentSnapshot | Change<DocumentSnapshot>>
}
