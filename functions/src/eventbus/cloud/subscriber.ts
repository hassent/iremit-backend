import * as functions from 'firebase-functions';
import {Topic} from './topic';
import * as request from 'request-promise';
import {Type} from '../../model/Event';
import { UserService } from '../../service/UserService';
import container from '../../provider/inversify.config';
import TYPES from '../../provider/Types';
import { UserStatus } from '../../model/User';


class Injectables {
    static USER: UserService = container.get<UserService>(TYPES.UserService);
}

export const userSubscriber = functions.pubsub.topic(Topic.UserTopic).onPublish((message, context) => {
    console.log("userSubscriber function triggered", message.json);
    return Promise.resolve(message);
});

export const roleSubscriber = functions.pubsub.topic(Topic.RoleTopic).onPublish(async (message, context) => {
    console.log("roleSubscriber function triggered", message.json);
    if(message.json.type === Type.Create) {
        const payload = message.json.data.role;
        return Promise.resolve(Injectables.USER.updateUserStatus(payload.id, UserStatus.ACTIVE));
    }
    return Promise.resolve(message);
});

export const remittanceSubscriber = functions.pubsub.topic(Topic.RemittanceTopic).onPublish((message, context) => {
    console.log("remittanceSubscriber function triggered", message.json);
    return Promise.resolve(message);
});

export const defaultSubscriber = functions.pubsub.topic(Topic.DefaultTopic).onPublish((message, context) => {
    console.log("defaultSubscriber function triggered", message.json);
    return Promise.resolve(message);
});

export const customerSubscriber = functions.pubsub.topic(Topic.CustomerTopic).onPublish((message, context) => {
    console.log("customerSubscriber function triggered", message.json);
    return Promise.resolve(message);
});
