import { RegistrableSubscriberService } from "./RegistrableSubscriberService";
import container from "../../provider/inversify.config";
import TYPES from "../../provider/Types";
import { SubscriberFunction } from "../cloud/SubscriberFunction";


const registrableEntityListeners: RegistrableSubscriberService[] = container.getAll<RegistrableSubscriberService>(TYPES.ServiceHandler);
registrableEntityListeners.forEach((entity) => {
    const subFuncs: SubscriberFunction[] = entity.subscribe();
    subFuncs.forEach(f => exports[f.functionName] = f.trigger)
});