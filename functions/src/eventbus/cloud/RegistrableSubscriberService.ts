import {SubscriberFunction} from "./SubscriberFunction";

export interface RegistrableSubscriberService {
    subscribe():SubscriberFunction[]
}