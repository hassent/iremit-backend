import * as PubSub from '@google-cloud/pubsub';

const projectId = 'iremit-isheger';
const pubsub = new PubSub({
    projectId: projectId
});

export class PublisherClient {

    public static publish(data: any, topic: string): Promise<boolean> {
        console.log("Publishing to ", topic);
        const dataBuffer = Buffer.from(JSON.stringify(data));
        return pubsub
            .topic(topic)
            .publisher()
            .publish(dataBuffer)
            .then((results) => {
                console.log("Successfull publish to ", topic);
                return true;
            })
    }

    public getClient(): PubSub {
        return pubsub;
    }
}
