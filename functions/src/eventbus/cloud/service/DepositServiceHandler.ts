import * as functions from 'firebase-functions';
import { Topic } from '../Topic';
import TYPES from '../../../provider/Types';
import { inject, injectable } from "inversify";
import { RegistrableSubscriberService } from "../RegistrableSubscriberService";
import { SubscriberFunction } from "../SubscriberFunction";
import { Event, Type } from "../../../model/Event";
import { Remittance, RemittanceStatus } from "../../../model/Remittance";
import { Deposit, DepositStatus } from "../../../model/Deposit";

@injectable()
export class DepositServiceHandler implements RegistrableSubscriberService {

    private readonly service: string = 'depositService_';

    private async onRemittanceUpdated(message, context) {
        console.log("Entered onRemittanceUpdated!", message.json);
        try {
            const event: Event = message.json;
            if (event.type === Type.Update) {
                const remittance = event.data.remittance;
                const delta = remittance.delta;
                if (delta.status && remittance.status === RemittanceStatus.PAID) {
                    //TODO
                    //update deposit balance for every individual who participated in this tx
                }
            }


        } catch (error) {
            console.warn(error);
        }
        return Promise.resolve();
    }

    private async onDepositUpdated(message, context) {
        console.log("Entered onRemittanceUpdated!", message.json);
        try {

            const event: Event = message.json;
            if (event.type === Type.Update) {
                const deposit = event.data.deposit;
                const delta = deposit.delta;
                if (delta.status && deposit.status === DepositStatus.APPROVED) {
                    //TODO
                    //update deposit balance for every individual who participated in this tx
                }
            }

        } catch (error) {
            console.warn(error);
        }
        return Promise.resolve();
    }


    subscribe(): SubscriberFunction[] {

        return [
            {
                functionName: this.service + "onRemittanceUpdated",
                trigger: functions.pubsub.topic(Topic.RemittanceTopic)
                    .onPublish(this.onRemittanceUpdated.bind(this))
            },
            {
                functionName: this.service + "onDepositUpdated",
                trigger: functions.pubsub.topic(Topic.DepositTopic)
                    .onPublish(this.onDepositUpdated.bind(this))
            }
        ];
    }

}






