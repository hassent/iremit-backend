import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import { RegistrableEntityListener } from "../RegistrableEntityListener";
import { EventParser } from "../EventParser";
import { MetaData, Type } from "../../../model/Event";
import { Topic } from "../../cloud/Topic";
import { PublisherClient as Publisher } from "../../cloud/Publisher";
import { SubscriberFunction } from "../../cloud/SubscriberFunction";
import { injectable } from 'inversify';

@injectable()
export class DepositListener implements RegistrableEntityListener {
    readonly listener: string = "deposit_";
    readonly path: string = "deposits/{id}";

    onCreate(snap, context) {
        console.log("onCreateDeposit() Triggered ", context.eventType);
        return EventParser.parse(Type.Create, MetaData.DepositEvent, snap.data())
            .then((result) => {
                return Publisher.publish(result, Topic.DepositTopic)
            }).catch((err) => {
                console.error(err.message);
            })
    }

    onUpdate(change, context) {
        console.log("onUpdateDeposit() Triggered ", context.eventType);
        return EventParser.parse(Type.Update, MetaData.DepositEvent, change.after.data())
            .then((result) => {
                result.data.delta = EventParser.delta(change);
                return Publisher.publish(result, Topic.DepositTopic)
            }).catch((err) => {
                console.error(err.message);
            })
    }

    onDelete(snap, context) {
        console.log("onDeleteDeposit() Triggered ", context.eventType);
        return EventParser.parse(Type.Delete, MetaData.DepositEvent, snap.data())
            .then((result) => {
                return Publisher.publish(result, Topic.DepositTopic)
            }).catch((err) => {
                console.error(err.message);
            })
    }

    subscribe(): SubscriberFunction[] {

        return [
            {
                functionName: this.listener + "onCreate",
                trigger: functions.firestore.document(this.path).onCreate(this.onCreate.bind(this))

            },
            {
                functionName: this.listener + "onUpdate",
                trigger: functions.firestore.document(this.path).onUpdate(this.onUpdate.bind(this))
            },
            {
                functionName: this.listener + "onDelete",
                trigger: functions.firestore.document(this.path).onDelete(this.onDelete.bind(this))
            }
        ];
    }

}