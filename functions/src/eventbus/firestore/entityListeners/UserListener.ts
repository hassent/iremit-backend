import * as functions from 'firebase-functions';
import { RegistrableEntityListener } from "../RegistrableEntityListener";
import { EventParser } from "../EventParser";
import { User } from "../../../model/User";
import { MetaData, Type } from "../../../model/Event";
import { Topic } from "../../cloud/Topic";
import { PublisherClient as Publisher } from "../../cloud/Publisher";
import { SubscriberFunction } from "../../cloud/SubscriberFunction";
import { injectable } from 'inversify';

@injectable()
export class UserListener implements RegistrableEntityListener {
    readonly listener: string = "user_";
    readonly path: string = "users/{id}";

    onCreate(snap, context) {
        console.log("onCreateUser() Triggered ", snap);
        const user: User = snap.data() as User;
        return EventParser.parse(Type.Create, MetaData.UserEvent, snap.data(), user.id, user.email)
            .then((result) => {
                return Publisher.publish(result, Topic.UserTopic)
            }).catch((err) => {
                console.error(err.message);
            })
    }

    onUpdate(change, context) {
        console.log("onUpdateUser() Triggered ", context.eventType);

        const user: User = change.after.data() as User;
        return EventParser.parse(Type.Update, MetaData.UserEvent, change.after.data(), user.id, user.email)
            .then((result) => {
                result.data.delta = EventParser.delta(change);
                return Publisher.publish(result, Topic.UserTopic)
            }).catch((err) => {
                console.error(err.message);
            })
    }

    onDelete(snap, context) {
        console.log("onDeleteUser() Triggered ", context.eventType);
        const user: User = snap.data() as User;
        return EventParser.parse(Type.Delete, MetaData.UserEvent, snap.data(), user.id, user.email)
            .then((result) => {
                return Publisher.publish(result, Topic.UserTopic)
            }).catch((err) => {
                console.error(err.message);
            })
    }

    subscribe(): SubscriberFunction[] {

        return [
            {
                functionName: this.listener + "onCreate",
                trigger: functions.firestore.document(this.path).onCreate(this.onCreate.bind(this))

            },
            {
                functionName: this.listener + "onUpdate",
                trigger: functions.firestore.document(this.path).onUpdate(this.onUpdate.bind(this))
            },
            {
                functionName: this.listener + "onDelete",
                trigger: functions.firestore.document(this.path).onDelete(this.onDelete.bind(this))
            }
        ];
    }

}