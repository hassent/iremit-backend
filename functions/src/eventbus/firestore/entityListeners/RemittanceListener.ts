import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import { RegistrableEntityListener } from "../RegistrableEntityListener";
import { EventParser } from "../EventParser";
import { Remittance } from "../../../model/Remittance";
import { MetaData, Type } from "../../../model/Event";
import { Topic } from "../../cloud/Topic";
import { PublisherClient as Publisher } from "../../cloud/Publisher";
import { SubscriberFunction } from "../../cloud/SubscriberFunction";
import { injectable } from 'inversify';

@injectable()
export class RemittanceListener implements RegistrableEntityListener {
    readonly listener: string = "remittance_";
    readonly path: string = "remittances/{id}";

    onCreate(snap, context) {
        console.log("onCreateRemittance() Triggered ", context.eventType);
        const remittance: Remittance = snap.data() as Remittance;
        return EventParser.parse(Type.Create, MetaData.RemittanceEvent, remittance)
            .then((result) => {
                return Publisher.publish(result, Topic.RemittanceTopic)
            }).catch((err) => {
                console.error(err.message);
            })
    }

    onUpdate(change, context) {
        console.log("onUpdateRemittance() Triggered ", context.eventType);
        const remittance: Remittance = change.after.data() as Remittance;
        return EventParser.parse(Type.Update, MetaData.RemittanceEvent, remittance)
            .then((result) => {
                result.data.delta = EventParser.delta(change);
                return Publisher.publish(result, Topic.RemittanceTopic)
            }).catch((err) => {
                console.error(err.message);
            })
    }

    onDelete(snap, context) {
        console.log("onDeleteRemittance Triggered ", context.eventType);
        const remittance: Remittance = snap.data() as Remittance;
        return EventParser.parse(Type.Delete, MetaData.RemittanceEvent, remittance)
            .then((result) => {
                return Publisher.publish(result, Topic.RemittanceTopic)
            }).catch((err) => {
                console.error(err.message);
            })
    }



    subscribe(): SubscriberFunction[] {

        return [
            {
                functionName: this.listener + "onCreate",
                trigger: functions.firestore.document(this.path).onCreate(this.onCreate.bind(this))

            },
            {
                functionName: this.listener + "onUpdate",
                trigger: functions.firestore.document(this.path).onUpdate(this.onUpdate.bind(this))
            },
            {
                functionName: this.listener + "onDelete",
                trigger: functions.firestore.document(this.path).onDelete(this.onDelete.bind(this))
            }
        ];
    }

}