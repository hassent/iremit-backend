import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import { RegistrableEntityListener } from "../RegistrableEntityListener";
import { EventParser } from "../EventParser";
import { MetaData, Type } from "../../../model/Event";
import { Topic } from "../../cloud/Topic";
import { PublisherClient as Publisher } from "../../cloud/Publisher";
import { SubscriberFunction } from "../../cloud/SubscriberFunction";
import { injectable } from 'inversify';
import { Customer } from '../../../model/Customer';

@injectable()
export class CustomerListener implements RegistrableEntityListener {
    readonly listener: string = "customer_";
    readonly path: string = "customers/{id}";

    onCreate(snap, context) {
        console.log("onCreateCustomer() Triggered ", context.eventType);
        const customer: Customer = snap.data() as Customer;
        return EventParser.parse(Type.Create, MetaData.CustomerEvent, customer, customer.ownerId)
            .then((result) => {
                return Publisher.publish(result, Topic.CustomerTopic)
            }).catch((err) => {
                console.error(err.message);
            })
    }

    onUpdate(change, context) {
        console.log("onUpdateCustomer() Triggered ", context.eventType);
        const customer: Customer = change.after.data() as Customer;
        return EventParser.parse(Type.Update, MetaData.CustomerEvent, customer, customer.ownerId)
            .then((result) => {
                result.data.delta = EventParser.delta(change);
                return Publisher.publish(result, Topic.CustomerTopic)
            }).catch((err) => {
                console.error(err.message);
            })
    }

    onDelete(snap, context) {
        console.log("onDeleteCustomer() Triggered ", context.eventType);
        const customer: Customer = snap.data() as Customer;
        return EventParser.parse(Type.Delete, MetaData.CustomerEvent, customer, customer.ownerId)
            .then((result) => {
                return Publisher.publish(result, Topic.CustomerTopic)
            }).catch((err) => {
                console.error(err.message);
            })
    }

    subscribe(): SubscriberFunction[] {

        return [
            {
                functionName: this.listener + "onCreate",
                trigger: functions.firestore.document(this.path).onCreate(this.onCreate.bind(this))

            },
            {
                functionName: this.listener + "onUpdate",
                trigger: functions.firestore.document(this.path).onUpdate(this.onUpdate.bind(this))
            },
            {
                functionName: this.listener + "onDelete",
                trigger: functions.firestore.document(this.path).onDelete(this.onDelete.bind(this))
            }
        ];
    }

}