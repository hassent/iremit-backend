import { RegistrableEntityListener } from "./RegistrableEntityListener";
import container from "../../provider/inversify.config";
import TYPES from "../../provider/Types";
import { SubscriberFunction } from "../cloud/SubscriberFunction";


const registrableEntityListeners: RegistrableEntityListener[] = container.getAll<RegistrableEntityListener>(TYPES.EntityListener);
registrableEntityListeners.forEach((entity) => {
    const subFuncs: SubscriberFunction[] = entity.subscribe();
    subFuncs.forEach(f => exports[f.functionName] = f.trigger)
});