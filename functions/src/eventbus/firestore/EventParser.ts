import * as functions from 'firebase-functions';
import { diff } from 'deep-object-diff';
import { Event, MetaData, Type } from '../../model/Event';
import * as admin from 'firebase-admin';
import DocumentSnapshot = FirebaseFirestore.DocumentSnapshot;



const eventCol: string = "events";


export class EventParser {
    static async parse(type: Type, meta: any, data: any, userId?: string, email?: string): Promise<Event> {

        const docRef = admin.firestore().collection(eventCol).doc();
        const event: Event = {
            id: docRef.id,
            name: meta.eventName,
            type: type,
            userId: userId ? userId : null,
            email: email ? email : null,
            triggeredBy: 'System User',
            timestamp: new Date(),
            data: { [meta.dataName]: data }
        };
        await docRef.set(event);
        return event
    }

    static delta(change: functions.Change<DocumentSnapshot>): object {
        return diff(change.before.data(), change.after.data());
    }


}