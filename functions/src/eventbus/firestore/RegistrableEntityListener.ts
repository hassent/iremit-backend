import { RegistrableSubscriberService } from "../cloud/RegistrableSubscriberService";

export interface RegistrableEntityListener extends RegistrableSubscriberService {
    listener: string;
    path: string;
    onCreate(snap, context);
    onUpdate(change, context);
    onDelete(snap, context);
}